# inout.conf - Example configuration file for SLB.
# Copyright (C) 2011 Sergey Poznyakoff
# License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
#
# This configuration illustrates load balancing of two servers based on
# the value of I/O data transfer rate on their network interfaces.
#
# See also inout-m4.conf - the same configuration using some m4 magic to
# accomodate a large number of servers.

wakeup 30;
pidfile "/tmp/slb-inout.pid";

# Compute actual transfer rate on the network interface.  The variables
# in and out refer to SNMP variables returning number of input and
# output bytes transferred through the interface (see their definition
# in the server blocks below.  The d() function computes the derivative
# of its argument with respect to time, i.e., in this case, the actual
# data rate on the interface in bytes per second.
expression traffic "d(in + out)";

# Output format string.  Prints the server ID and its load weight.
output-format "%i %w\n";

######################################
# Server definitions.
#
# Add as many of these, as needed.
######################################

server "srv1" {
  host "192.168.0.1";
  community "public";
  variable in "IF-MIB::ifInOctets.3";
  variable out "IF-MIB::ifOutOctets.3";
  expression "@traffic";
}

server "srv2" {
  host "192.168.0.2";
  community "public";
  variable in "IF-MIB::ifInOctets.3";
  variable out "IF-MIB::ifOutOctets.3";
  expression "@traffic";
}

# End of inout.conf
