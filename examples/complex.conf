# complex.conf - Example configuration file for SLB.
# Copyright (C) 2011 Sergey Poznyakoff
# License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
#
# This configuration illustrates load balancing of two servers based on
# a complex criterion.
#
# See also complex-m4.conf - the same configuration using some m4 magic to
# accomodate a large number of servers.
#
wakeup 30;
pidfile "/tmp/slb-complex.pid";

# The relative load weight is computed as a square root from the sum
# of the squared data transfer rate on the interface and the 1-minute
# load average of the server, each of them multiplied by a corresponding
# coefficient.
# The variables in, out and la1 as well as constants k and m are defined
# in the server statements below.
expression load "sqrt(k * d(in + out)**2 + m * la1**2)";

# Output format string.  Prints the server ID and its load weight.
output-format "%i %w\n";

######################################
# Server definitions.
#
# Add as many of these, as needed.
######################################

server "srv1" {
  host "192.168.0.1";
  community "public";
  # Declare the SNMP variables for use in calculations.
  table iftable IF-MIB::ifDescr;
  variable in "IF-MIB::ifInOctets.$iftable[eth0]";
  variable out "IF-MIB::ifOutOctets.$iftable[eth0]";
  variable la1 "UCD-SNMP-MIB::laLoadFloat.1";
  # Declare the coefficients.
  constant k 1.5;
  constant m 1.8;
  
  expression "@load";
}

server "srv2" {
  host "192.168.0.2";
  community "public";
  table iftable IF-MIB::ifDescr;
  variable in "IF-MIB::ifInOctets.$iftable[eth0]";
  variable out "IF-MIB::ifOutOctets.$iftable[eth0]";
  variable la1 "UCD-SNMP-MIB::laLoadFloat.1";

  constant k 1.5;
  constant m 1.8;
  
  expression "@load";
}

# End of complex.conf
