.\" This file is part of SLB. -*- nroff -*-
.\" Copyright (C) 2011 Sergey Poznyakoff
.\"
.\" SLB is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" SLB is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with SLB.  If not, see <http://www.gnu.org/licenses/>.
.TH SLB "8" "June 23, 2011" "SLB" ""
.SH NAME
\fBslb\fR \- Simple Load Balancer

.SH SYNOPSIS
\fBslb\fR [\-EVehnt] [\-D \fISYMBOL[=VALUE]\fR] [\-I \fIDIR\fR]\
 [\-o \fIFILE\fR] [\-c \fIFILE\fR] [\-d \fICAT\fR[.\fILEVEL\fR]] [--config-file=\fIFILE\fR]\
 [\-\-config-help] [\-\-cron] [\-\-debug=\fICAT\fR[.\fILEVEL\fR]] [\-\-define=\fISYMBOL[=VALUE]\fR]\
 [\-\-dry-run] [\-\-foreground]\
 [\-\-help] [\-\-include-directory=\fIDIR\fR] [\-\-lint] [\-\-no-preprocessor]\
 [\-\-output-file=\fIFILE\fR] [\-\-preprocessor=\fICOMMAND\fR] [\-\-stderr]\
 [\-\-syslog] [\-\-usage] [\-\-version]
.br
\fBslb\fR \-\-eval=\fINAME\fR [variable=\fIvalue\fR...]
.br
\fBslb\fR \-T [\fIFILE\fR]
.br
\fBslb\fR \-\-test [\fIFILE\fR]

.SH DESCRIPTION
\fBSlb\fR is a tool designed to simplify the task of load balancing
between a set of servers.  It monitors a set of servers and uses SNMP
protocol to periodically collect a set of parameters from each of them.
Once obtained, these parameters are used as arguments to a \fIload
estimation function\fR, which computes, for each server, a floating
point value representing its \fIrelative load\fR. \fBSlb\fR then sorts the
servers in the order of increasing relative load.

\fBSlb\fR does not attempt to actually correct the load distribution, as
this task depends heavily on the kind of work the servers are
performing and it is difficult, if not impossible, to provide a
generalized solution for that. Instead, \fBslb\fR relies on an external
program which is supposed to redistribute the load, based on the data
obtained from it. Therefore, after obtaining the sorted list of
servers, \fBslb\fR filters it through a user-defined format template, and
sends the result to a file, named pipe or another program.

All parameters used by \fBslb\fR, including load estimation function, are
supplied to it in a \fIconfiguration file\fR, which makes the package
extremely flexible.

This manpage is a short description of the \fBslb\fR daemon.
For a detailed discussion, including examples of the configuration and
usage recommendation, refer to the \fBSLB User Manual\fR available in
Texinfo format.  To access it, run:

  \fBinfo slb\fR

Should any discrepancies occur between this manpage and the
\fBSLB User Manual\fR, the later shall be considered the authoritative
source.

.SH OPTIONS
.TP
\fB-c\fR \fIFILE\fR, \fB\-\-config-file\fR=\fIFILE\fR
Read \fIFILE\fR instead of the default configuration file.
.TP
\fB\-\-cron\fR
Cron mode: run load estimation loop once and exit.
.TP
\fB\-\-eval\fR=\fINAME\fR
Evaluate the expression \fINAME\fR, print its result and exit.
The expression must be defined in the configuration file with
the \fBexpression\fR statement (see \fBCONFIGURATION FILE\fR below).
Actual values for the variables used in the expression can be supplied
in the form of assignments in the command line, e.g.:

  slb --eval=loadavg la1=10 outbytes=1800

Several values can be supplied for a single variable as in:

  slb --eval=loadavg la1=10,11 outbytes=1800,3000,4500

In this case the expression will be evaluated as many times as the
maximum number of values, the result of each evaluation being
printed to the standard output.  In the example above, the expression
is evaluated three times, with the following values as input:

  la1=10  outbytes=1800
  la1=11  outbytes=3000
  la1=11  outbytes=4500

This form is useful if the expression contains the \fBd\fR() function,
which requires at least a pair of data to compute a meaningful value.
.TP
\fB\-E\fR
Preprocess config and exit.
.TP
\fB\
.TP
\fB\-\-foreground\fR
Do not detach from the controlling terminal, run in foreground.
.TP
\fB\-n\fR, \fB\-\-dry-run\fR
Run in foreground mode and print on the standard output what would
have otherwise been printed on the output file.  This option implies
\fB\-\-stderr \-\-debug snmp.1 \-\-debug output.1\fR.  Use additional
\fB\-\-debug\fR options to get even more info.
.TP
\fB\-t\fR, \fB\-\-lint\fR
Parse the configuration file and exit.
.TP
\fB-T\fR, \fB\-\-test\fR
Read SNMP variables and their values from a file and act as if they
were returned by SNMP.  The output is directed to the standard output,
unless the \fB\-\-output-file\fR option is also given.

The input file name is taken from the first non-option argument.  If
it is \fB-\fR (a dash) or if no arguments are given, the standard
input will be read.

The input file consists of sections separated by exactly one empty
line.  A section contains assignments to SNMP variables in the style
of \fBsnmpset(1)\fR.  Such assignments form several server groups, each
group being preceded by a single word on a line, followed by a
semicolon.  This word indicates the \fBID\fR of the server to which
the data in the group belong.  For example:

  srv01:
  UCD-SNMP-MIB::laLoadFloat.1 F 0.080000
  IF-MIB::ifOutUcastPkts.3 c 346120120
  srv02:
  UCD-SNMP-MIB::laLoadFloat.1 F 0.020000
  IF-MIB::ifOutUcastPkts.3 c 2357911693

This section supplies data for two servers, named \(aqsrv01\(aq and
\(aqsrv02\(aq.
.TP
\fB\-o\fR, \fB\-\-output-file\fR=\fIFILE\fR
Direct output to \fIFILE\fR.  This option overrides the
\fBoutput-file\fR configuration statement (see below).  The format of
\fIFILE\fR argument is the same as for that statement.
.SS Logging
.TP
\fB-e\fR, \fB--stderr\fR
Log to stderr.
.TP
\fB\-\-syslog\fR
Log to syslog.
.SS Preprocessor control
.TP
\fB\-D\fR \fISYMBOL[=VALUE]\fR, \fB\-\-define\fR=\fISYMBOL[=VALUE]\fR
Define a preprocessor symbol.
.TP
\fB\-I\fR \fIDIR\fR, \fB\-\-include-directory\fR=\fIDIR\fR
Add directory \fIDIR\fR to the \fBm4\fR include search path.
.TP
\fB\-\-no-preprocessor\fR
Disable preprocessing.
.TP
\fB\-\-preprocessor\fR=\fICOMMAND\fB
Use \fICOMMAND\fR instead of the default preprocessor.
.SS Debugging
.TP
\fB\-d \fICAT\fR[.\fILEVEL\fR], \fB\-\-debug\fR=\fICAT\fR[.\fILEVEL\fR]
Sets debugging level \fILEVEL\fR for the category \fICAT\fR.

\fILEVEL\fR is a decimal number in the range 0..100.  If omitted, 100
is assumed.

Available categories are: \fBmain\fR, \fBeval\fR, \fBegram\fR,
\fBelex\fR, \fBsnmp\fR, \fBoutput\fR, \fBcfgram\fR and \fBcflex\fR.
.SS Help and additional information
.TP
\fB\-\-config-help\fR
Show a summary of the configuration file syntax.
.TP
\fB\-V\fR, \fB\-\-version\fR
Print program version and exit.
.TP
\fB\-h\fR, \fB\-\-help\fR
Give a short help summary.
.TP
\fB\-\-usage\fR
Print a short usage message.

.SH CONFIGURATION FILE
.SS Daemon configuration
.TP
\fBdaemon\fR \fIBOOL\fR;
Run in daemon mode (if \fIBOOL\fR is \fByes\fR).
.TP
\fBforeground\fR \fIBOOL\fR;
Do not detach from the controlling terminal.
.TP
\fBpidfile\fR \fIFILE\fR;
Store master process PID in \fIFILE\fR.  The default pidfile
location is \fB/var/run/slb.pid\fR.
.TP
\fBexpression\fR \fINAME\fR \fIEXPR\fR;
Define a named expression.  See \fIEXPRESSION SYNTAX\fR below.
.SS Syslog configuration
.TP
\fBsyslog\fR { ... }
Configures syslog:

  \fBsyslog\fR {
    \fBfacility\fR \fINAME\fR;
    \fBtag\fR \fISTRING\fR;
    \fBprint-priority\fR \fIBOOL\fR;
  }
.TP
\fBfacility\fR \fINAME\fR;
Configures the syslog facility to use. Allowed values are:
auth, authpriv, cron, daemon, ftp, local0 through local7, and mail.
.TP
\fBtag\fR \fISTRING\fR;
Sets the \fBsyslog tag\fR, a string identifying each message issued by
the program. By default, it is the name of the program with the directory
parts removed.
.TP
\fBprint-priority \fIBOOL\fR;
Controls whether syslog messages are prefixed with the corresponding
priority.
.SS SNMP configuration
.TP
\fBwakeup\fR \fINUMBER\fR;
Sets wake-up interval in seconds.  \fBSlb\fR recomputes server load
table each \fInumber\fR seconds.
.TP
\fBsuppress-output\fR \fINUMBER\fR;
Suppress output during the first \fINUMBER\fR wakeups.  This statement
is reserved mostly for debugging purposes.
.TP
\fBmib-directory\fR \fIDIR\fR
Adds \fIDIR\fR to the list of directories searched for the MIB
definition files.
.TP
\fBadd-mib\fR \fIFILE\fR;
Reads MIB definitions from \fIFILE\fR.
.SS Server Definition
  \fBserver\fR \fIid\fR {
    \fBhost\fR \fIstring\fR;
    \fBtimeout\fR \fInumber\fR;
    \fBretries\fR \fInumber\fR;
    \fBcommunity\fR \fIstring\fR;
    \fBexpression\fR \fIexpr\fR;
    \fBvariable\fR \fIname\fR [\fIoid\fR [\fIorder\fR]];
    \fBconstant\fR \fIname\fR \fIvalue\fR;
    \fBmacro\fR \fIname\fR \fIexpansion\fR;
    \fBassert\fR \fIoid\fR \fIop\fR \fIvalue\fR;
  }

The \fBserver\fR creates a new entry in the database of monitored servers.
The \fIid\fR parameter supplies the \fBserver identifier\fR, an arbitrary string
which will be used in log messages related to that server.  Its value
is also available in output format string as format specifier
\fB%i\fR (see \fBOUTPUT FORMAT\fR below).
  
.TP
\fBhost\fR \fISTRING\fR;
Sets the IP address (or hostname) of this server.
.TP
\fBtimeout\fR \fINUMBER\fR;
Sets the timeout for SNMP requests to that server.
.TP
\fBretries\fR \fINUMBER\fR;
Sets the maximum number of retries for that server, after which a
timeout is reported.
.TP
\fBcommunity\fR \fISTRING\fR;
Sets SNMP community.
.TP
\fBexpression\fR \fISTRING\fR;
Defines expression to compute relative load average of this server.
See the section \fBEXPRESSION SYNTAX\fR, for a discussion of its syntax.
.TP
\fBvariable\fR \fINAME\fR \fIOID\fR [\fIORDER\fR];
Defines a variable \fINAME\fR to have the value returned by
SNMP \fIOID\fR.  The latter must return a numeric value.

Any occurrence of \fINAME\fR in the expression (as defined in the
\fBexpression\fR statement) is replaced with this value.

Optional \fIORDER\fR specifier describes the order in which this
variable changes with time. The value \fBincreasing\fR means that the
values of this variable are described by an increasing (more properly,
non-decreasing) function. The value \fBdecreasing\fR declares this
variable as a decreasing (more strictly, non-increasing) function of
time.

The \fIORDER\fR specifier is applicable only to oids that return unsigned
integer values.  When supplied, it instructs \fBslb\fR to handle any
changes in the value of this variable that don't match the declared
order as integer overflows.
.TP
\fBvariable\fR \fINAME\fR;
This is a special form of the \fBvariable\fR statement.  It declares an
\fIauxiliary variable\fR, which must be initialized in the expression
itself.  See \fBAssignments\fR, for more information. 
.TP
\fBconstant\fR \fINAME\fR \fINUMBER\fR
Defines a constant \fINAME\fR to have the value returned by
\fIOID\fR.

Notice, that expression variables and constants share the same
namespace, therefore it is an error to have both a \fBvariable\fR and
a \fBconstant\fR statement defining the same \fINAME\fR.
.TP
\fBmacro\fR \fINAME\fR \fIEXPANSION\fR
Defines a macro \fINAME\fR to expand to \fIEXPANSION\fR.  Macros allow
to customize output formats on a per-server basis.
.TP
\fBassert\fR \fIOID\fR \fIOP\fR \fIPATTERN\fR
Ensures that the value of SNMP variable \fIOID\fR matches
\fIPATTERN\fR.  The type of match is given by the \fIOP\fR argument:
\fBeq\fR means that the value must match \fIPATTERN\fR exactly and
\fBne\fR means that the value must not match \fIPATTERN\fR.

If the assertion fails, the server is excluded from the output table.
.SS Output control
.TP
\fBhead\fR \fINUMBER\fR;
Print at most \fINUMBER\fR entries from the top of the table, i.e. the
\fINUMBER\fR of the less loaded server entries.
.TP
\fBtail\fR \fINUMBER\fR;
Print at most \fINUMBER\fR entries from the bottom of the table, i.e. the
\fINUMBER\fR of the most loaded server entries.
.TP
\fBoutput-file\fR \fISTRING\fR;
Send output to the channel, identified by \fISTRING\fR.  Unless
\fISTRING\fR starts with a pipe sign, it is taken as a literal name of
the file.  If this file does not exist, it is created.  If it
exists, it is truncated.  The file name can refer to a regular file,
symbolic link or named pipe.

If \fBstring\fR starts with a \fB|\fR (pipe), the rest of the string
is taken as the name of an external program and its command line
arguments. The program is started before \fBslb\fR enters its
main loop and the formatted load table is piped on its standard input.
.TP
\fBbegin-output-message\fR \fISTRING\fR;
Defines the text to be output before the actual load table contents.
The \fISTRING\fR is taken literally.  For example, if you want it to
appear on a line by itself, put \fB\\n\fR at the end of it.
.TP
\fBend-output-message\fR \fISTRING\fR;
Defines the text to be output after the actual load table contents.
The \fISTRING\fR is taken literally.
.TP
\fBoutput-format\fR \fISTRING\fR;
Defines the \fIformat\fR for outputting load table entries.  See
\fBOUTPUT FORMAT\fR, for a description of available format specifications.

.SH EXPRESSION SYNTAX
\fIExpression\fR is an arithmetical statement, which evaluates to a
floating point number.  The expression consists of \fBterms\fR,
\fBfunction calls\fR, \fBsubexpression references\fR and
operators. The \fBterms\fR are floating point numbers, variable and
constant names. The names refer to constants or SNMP variables defined
in the definition of the server, for which this expression is being
evaluated.

Expressions implement the usual four arithmetic operations:
.TP
\fB+\fR
Addition.
.TP
\fB\-\fR
Subtraction.
.TP
\fB*\fR
Multiplication.
.TP
\fB/\fR
Division.
.TP
\fB**\fR
Power.
.PP
A \fBsubexpression reference\fR is a construct in the form:

  @\fBexpr\fR

where \fBexpr\fR is the name of the expression defined elsewhere in
the configuration file (see the description of \fBexpression\fR statement).
.PP
The \fIternary conditional operator\fR selects a value based
on a condition.  It has the following syntax:

  \fBcond\fR ? \fBexpr1\fR : \fBexpr2\fR

The ternary operator evaluates to \fBexpr1\fR if \fBcond\fR yields
true (i.e. returns a non-null value) and to \fBexpr2\fR otherwise.

The condition \fBcond\fR is an expression which, apart from the
arithmetic operators above, can use the following \fIcomparison\fR and
\fIlogical\fR operations:
.SS Comparisons
.TP
\fBa\fR = \fBb\fR
True if \fBa\fR equals \fBb\fR
.TP
\fBa\fR != \fBb\fR
True if the operands are not equal.
.TP
\fBa\fR < \fBb\fR
True if \fBa\fR is less than \fBb\fR.
.TP
\fBa\fR <= \fBb\fR
True if \fBa\fR is less than or equal to \fBb\fR.
.TP
\fBa\fR > \fBb\fR
True if \fBa\fR is greater than \fBb\fR.
.TP
\fBa\fR >= \fBb\fR
True if \fBa\fR is greater than or equal to \fBb\fR.
.SS Logical operations
.TP
!\fBexpr\fR
True if \fBexpr\fR is false.
.TP
\fBa\fR && \fBb\fR
Logical \fBAND\fR: true if both \fBa\fR and \fBb\fR are true, false otherwise.
.TP
\fBa\fR || \fBb\fR
Logical \fBOR\fR: true if at least one of \fBa\fR or \fBb\fR are true.
.PP
Both logical \fBAND\fR and \fBOR\fR implement \fIboolean shortcut
evaluation\fR: their second argument (\fBb\fR) is evaluated only when
the first one is not sufficient to compute the result.
.SS Functions
Function calls have the following syntax:

  \fBname\fR(\fBarglist\fR)

where \fBname\fR stands for the function name and \fBarglist\fR
denotes the \fIargument list\fR: a comma-separated list of expressions
which are evaluated and supply actual arguments for the function.

The following functions are supported:
.TP
\fBd\fR(\fIx\fR)
Returns the derivative of \fIx\fR, i.e. the speed of its change per
second, measured between the two last wakeups of \fBslb\fR.

Notice, that this function is available only in daemon mode.
.TP
\fBmax\fR(\fIx0\fR, ..., \fIxn\fR)
Returns the maximum value of its arguments. Any number of arguments
can be given.
.TP
\fBmin\fR(\fIx0\fR, ..., \fIxn\fR)
Returns the minimum value of its arguments.
.TP
\fBavg\fR(\fIx0\fR, ..., \fIxn\fR)
Returns the average value of its arguments.
.TP
\fBlog\fR(\fIx\fR)
Returns the natural logarithm of \fIx\fR.
.TP
\fBlog10\fR(\fIx\fR)
Returns the decimal logarithm of \fIx\fR.
.TP
\fBexp\fR(\fIx\fR)
Returns the value of \fBe\fR (the base of natural logarithms) raised to
the power of \fIx\fR.
.TP
\fBpow\fR(\fIx\fR, \fIy\fR)
Returns the value of \fIx\fR raised to the power of \fBy\fR.
.TP
\fBsqrt\fR(\fIx\fR)
Returns the non-negative square root of \fIx\fR.
.TP
\fBabs\fR(\fIx\fR)
Returns the absolute value of \fIx\fR.
.TP
\fBceil\fR(\fIx\fR)
Returns the smallest integral value that is not less than \fIx\fR.

  ceil(0.5) => 1.0
  ceil(-0.5) => 0
.TP
\fBfloor\fR(\fIx\fR)
Returns the largest integral value that is not greater than \fIx\fR.

  floor(0.5) => 0.0
  floor(-0.5) => -1.0
.TP
\fBtrunc\fR(\fIx\fR)
Rounds \fIx\fR to the nearest integer not larger in absolute value.
.TP
\fBround\fR(\fIx\fR)
Rounds \fIx\fR to the nearest integer, but round halfway cases away
from zero:

  round(0.5) => 1.0
  round(-0.5) => -1.0
.SS Assignments
Assignment operator (\fB=\fR) initializes the variable which
appears at its left hand side with the result of the expression that
appears at its right. The value of this operator is the value stored
in the left operand after the assignment has taken place. The variable
must be declared in the corresponding \fBserver\fR block as an
auxiliary variable.

Auxiliary variables provide a mechanism of keeping a value of a
subexpression for further use, either in the same expression where
they are initialized, or in the output format specification.  For
example:

  expression "(x = d(in)) / max_diff";
  output-format "%w (%{x})";

.SS Comma operator
A sequence of expressions separated by commas is evaluated left to
right.  Its value is that of the rightmost expression.  Example:

  expression <<EOT
      x = d(in),
      (x > maxinput) ? x : x / maxinput
  EOT;    

.SH OUTPUT FORMAT
The format string is composed of zero or more directives: ordinary
characters (not \fB%\fR), which are copied unchanged to the output;
and \fIconversion specifications\fR, each of which is replaced with the
result of a corresponding conversion.  Each conversion specification
is introduced by the character \fB%\fR, and ends with a
\fIconversion specifier\fR. In between there may be (in this order)
zero or more \fIflags\fR, an optional \fIminimum field width\fR and an
optional \fIprecision\fR.
.SS Flags
.TP
\fB0\fR
The value should be zero padded. This affects all floating-point
conversions, i.e. \fBw\fR and \fB{...}\fR, for which the resulting
value is padded on the left with zeros rather than blanks.  If the
\fB0\fR and \fB\-\fR flags both appear, the \fB0\fR flag is ignored.
If a precision is given with a floating-point conversion, the \fB0\fR flag
is ignored. For other conversions, the behavior is undefined.
.TP
\fB-\fR
The converted value is to be left adjusted on the field  boundary.
(The default is right justification.)  Normally, the converted
value is padded on the right  with blanks, rather than on the left
with blanks or zeros.  A \fB-\fR overrides a \fB0\fR if both are
given.        
.TP
.B \(aq \(aq
(a space) A blank should be left before a positive number
(or empty string) produced by a conversion.

.SS The field width
An optional decimal digit string (with non-zero first digit)
specifying a minimum field width.  If the converted value has fewer
characters than the field width, it will be padded with spaces on the
left (or right, if the \fB-\fR flag has been given).

In no case does a nonexistent or small field width cause truncation of
a field; if the result of a conversion is wider than the field width,
the field is expanded to contain the conversion result.
.SS The precision
An optional precision, in the form of a period (\fB.\fR) followed by
an optional decimal digit string.  If the precision is given  as just
\fB.\fR, or the precision is negative, the precision is taken to be
zero.  This gives the number of digits to appear after the radix
character for floating-point conversions or the maximum number of
characters to be printed from a string, for string conversions.
.SS The conversion specifier
A character or a sequence of characters that specifies the type of
conversion to be applied. The conversion specifiers are replaced in
the resulting output string as described below:
.TP
\fBi\fR
The server identifier.
.TP
\fBh\fR
The server hostname, as set by the \fBhost\fR configuration
directive.
.TP
\fBw\fR
Computed relative load.
.TP
\fB{\fIvar\fB}\fR
The value of the variable \fIvar\fR.
.TP
\fB{@\fIexpr-name\fB}\fR
The result of evaluating expression \fIexpr-name\fR in the context of
the current server.
.TP
\fB(\fImacro\fB)\fR
Expansion of the \fImacro\fR.
.TP
\fB%\fR
The percent character.

.SH SIGNALS
The program handles a set of signals.  To send a signal to
\fBslb\fR use the following command:

  kill -\fBSIGNAL\fR `cat /var/run/slb.pid`

Replace \fI/var/run/slb.pid\fR with the actual pathname to the
PID-file, if it was set using the \fBpidfile\fR configuration
statement.

.TP
.B SIGHUP
Instructs the running instance of the program
to restart itself.  It is possible only if \fBslb\fR was started using
its full pathname.
.TP
.BR SIGTERM ", " SIGQUIT ", " \fBSIGINT\fR
\fBslb\fR terminates gracefully.

.SH EXIT CODES
The following exit codes are defined:
.RS
.PD 0
.TP 3
.I 0
Normal termination.
.TP 3
.I 64
Invalid command line usage.
.TP 3
.I 65
Input data were invalid or malformed.  This error code is returned
only when \fBslb\fR is used with \fB\-\-test\fR or
\fB\-\-eval\fR options.
.TP 3
.I 69
Some error occurred.  For example, the program was unable to open
output file, etc.
.TP 3
.I 70
Internal software error.  This usually means hitting a bug in the
program, so please report it.
.TP 3
.I 98
Program terminated due to errors in configuration file.
.PD
.RE
If a non-0 code is returned the exact cause of failure is
reported on the currently selected logging channel.

.SH AUTHORS
Sergey Poznyakoff

.SH "BUG REPORTS"
Report bugs to <gray+slb@gnu.org.ua>.

.SH COPYRIGHT
Copyright \(co 2011 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH SLB \"8\" \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 16
.\" end:

