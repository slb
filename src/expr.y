/* This file is part of SLB
   Copyright (C) 2011, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

%{
#include "slb.h"
#include "grecs/locus.h"
#include <math.h>

double round(double x);
double trunc(double x);


static struct grecs_locus_point current_point;
static const char *inbuf;
static const char *curp;
static int input_char;
static int yyeof;

struct slb_node_state {
	struct slb_node_state *next;
	void *node;
	void *key;
	size_t size;
	void *data;
	void (*initfn)(void *);
};

static struct grecs_symtab *parse_vartab;
static struct slb_node *parse_tree;

static int yylex(void);
static void yyerror(char *s);
static struct slb_node *alloc_node(struct grecs_locus_point beg,
				   struct grecs_locus_point end);
static struct function *function_lookup(const char *name, size_t len);

struct daemon_only_closure
{
	struct function *func;
	grecs_locus_t locus;
};

static int
request_daemon_mode_hook(void *ptr)
{
	struct daemon_only_closure *clos = ptr;

	if (!standalone_mode) {
		grecs_error(&clos->locus, 0,
			    "function %s can be used only in standalone mode",
			    clos->func->name);
		return 1;
	}
	return 0;
}

%}

%error-verbose
%locations

%token <number> T_NUMBER
%token <var> T_VARIABLE
%token <func> T_FUNC
%token T_ERR
%token <expr> T_EXPR

%left '='
%left T_OR
%left T_AND
%left T_NOT
%nonassoc T_EQ T_NE
%nonassoc T_LT T_LE T_GT T_GE
%left '+' '-'
%left '*' '/'
%left T_UMINUS
%right T_POW
%left '?'

%type <node> expr atom opte func asgn
%type <list> args argl elst

%union {
	double number;
	struct slb_varref *var;
	struct slb_node *node;
	struct function *func;
	struct slb_node_list list;
	struct { const char *name; size_t len; } expr;
};

%%

input: elst
       {
	       if ($1.count == 1)
		       parse_tree = $1.head;
	       else {
		       parse_tree = alloc_node(@1.beg, @1.end);
		       parse_tree->n_type = node_list;
		       parse_tree->n_next = $1.head;
	       }
       }
     ;

elst : expr
       {
	       $1->n_next = NULL;
	       $$.head = $$.tail = $1;
	       $$.count = 1;
       }
     | elst ',' expr
       {
	       $$.tail->n_next = $3;
	       $$.tail = $3;
	       $$.count++;
       }
     ;

expr : expr T_EQ expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_eq;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_NE expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_ne;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_LT expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_lt;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_LE expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_le;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_GT expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_gt;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_GE expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_ge;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_OR expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_or;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_AND expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_and;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr '+' expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_add;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr '-' expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_sub;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr '*' expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_mul;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr '/' expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_div;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | expr T_POW expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_pow;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
       }
     | '-' expr %prec T_UMINUS
       {
	       $$ = alloc_node(@1.beg, @2.end);
	       $$->n_type = node_uminus;
	       $$->v.n_arg[0] = $2;
       }
     | '+' expr %prec T_UMINUS
       {
	       $$ = $2;
       }
     | T_NOT expr
       {
	       $$ = alloc_node(@1.beg, @2.end);
	       $$->n_type = node_not;
	       $$->v.n_arg[0] = $2;
       }
     | '(' expr ')'
       {
	       $$ = $2;
       }
     | expr '?' opte ':' expr %prec '?'
       {
	       $$ = alloc_node(@1.beg, @5.end);
	       $$->n_type = node_cond;
	       $$->v.n_arg[0] = $1;
	       $$->v.n_arg[1] = $3;
	       $$->v.n_arg[2] = $5;
       }
     | func
     | atom
     | asgn
     ;

asgn : T_VARIABLE '=' expr
       {
	       $$ = alloc_node(@1.beg, @3.end);
	       $$->n_type = node_asgn;
	       $$->v.n_asgn.var = $1;
	       $$->v.n_asgn.expr = $3;
       }
     ;

opte : /* empty */
       {
	       $$ = NULL;
       }
     | expr
     ;

atom : T_NUMBER
       {
	       $$ = alloc_node(@1.beg, @1.end);
	       $$->n_type = node_const;
	       $$->v.n_const = $1;
       }
     | T_VARIABLE
       {
	       $$ = alloc_node(@1.beg, @1.end);
	       $$->n_type = node_variable;
	       $$->v.n_var = $1;
       }
     | T_EXPR
       {
	       $$ = alloc_node(@1.beg, @1.end);
	       $$->n_type = node_subexpr;
	       $$->v.n_expr.name = grecs_malloc($1.len + 1);
	       memcpy($$->v.n_expr.name, $1.name, $1.len + 1);
	       $$->v.n_expr.name[$1.len] = 0;
       }
     ;

func : T_FUNC '(' args ')'
       {
	       if ($3.count < $1->minargs) {
		       grecs_error(&@1, 0, "too few arguments in call to %s",
				   $1->name);
		       YYERROR;
	       } else if ($3.count > $1->maxargs) {
		       grecs_error(&@1, 0, "too many arguments in call to %s",
				   $1->name);
		       YYERROR;
	       } 
	       if ($1->flags & FUN_DAEMON_ONLY) {
		       struct daemon_only_closure *p = grecs_malloc(sizeof(*p));
		       p->func = $1;
		       p->locus = @1;
		       add_config_finish_hook(request_daemon_mode_hook, p);
	       }

	       $$ = alloc_node(@1.beg, @4.end);
	       $$->n_type = node_func;
	       $$->n_min_eval = $1->min_eval;
	       $$->v.n_func.func = $1->func;
	       $$->v.n_func.args = $3.head;
	       $$->v.n_func.nargs = $3.count;
       }
     ;

args : /* empty */
       {
	       $$.head = $$.tail = NULL;
	       $$.count = 0;
       }
     | argl
     ;

argl : expr
       {
	       $1->n_next = NULL;
	       $$.head = $$.tail = $1;
	       $$.count = 1;
       }
     | argl ',' expr
       {
	       $1.tail->n_next = $3;
	       $1.tail = $3;
	       $1.count++;
	       $$ = $1;
       }
     ;

%%

void
yyerror(char *s)
{
	grecs_error(&yylloc, 0, "%s", s);
}

static int
input()
{
	if (yyeof)
		input_char = 0;
	else if (curp) {
		input_char = *curp;
		if (!input_char) 
			yyeof++;
		else {
			curp++;
			if (input_char == '\n') {
				current_point.line++;
				current_point.col = 1;
			} else
				current_point.col++;
		}
	}
	return input_char;
}

static void
unput()
{
	if (yyeof)
		return;
	if (curp > inbuf)
		--curp;
	if (curp > inbuf)
		input_char = curp[-1];
	if (input_char == '\n') {
		const char *p;
		
		current_point.line--;
		for (p = curp, current_point.col = 1; p > inbuf && *p != '\n';
		     current_point.col++);
	} else
		current_point.col--;
}

static int
yylex()
{
	/* Skip whitespace */
	while (input() && isspace(input_char))
		;
	if (!input_char)
		return 0;

	yylloc.beg = current_point;
	yylloc.beg.col--;
	yylloc.end = yylloc.beg;
		
	/*
	 * A number
	 */
	if (isdigit(input_char)) {
		const char *p;
			
		yylval.number = strtod(curp - 1, (char**)&p);
		debug(SLB_DEBCAT_ELEX, 1,
		      ("yylex: NUMBER %f", yylval.number));
		current_point.col += p - curp;
		yylloc.end = current_point;
		curp = p;
		return T_NUMBER;
	}

	if (isalpha(input_char) || input_char == '_') {
		const char *p = curp - 1;
		size_t len = 1;
		struct function *fp;
		int install;
		
		while (input() &&
		       (isalpha(input_char) || input_char == '_' ||
			isdigit(input_char)))
			len++;
		unput();
		yylloc.end = current_point;
		
		fp = function_lookup(p, len);
		if (fp) {
			yylval.func = fp;
			debug(SLB_DEBCAT_ELEX, 1,
			      ("yylex: FUNC: %.*s", len, p));
			return T_FUNC;
		}

		install = 1;
		yylval.var = varref_lookup(parse_vartab, p, len, &install);
		debug(SLB_DEBCAT_ELEX, 1,
		      ("yylex: VARIABLE: %.*s", len, p));
		return T_VARIABLE;
	}

	if (input_char == '@') {
		const char *p = curp;
		size_t len = 0;
		
		while (input() &&
		       (isalpha(input_char) || input_char == '_' ||
			isdigit(input_char)))
			len++;
		unput();
		yylloc.end = current_point;
		
		yylval.expr.name = p;
		yylval.expr.len = len;
		return T_EXPR;
	}
	
	/*
	 * Boolean expressions
	 */
	if (input_char == '&' || input_char == '|') {
		int c = input_char;

		if (input() == c) {
			debug(SLB_DEBCAT_ELEX, 1,
			      ("yylex: %s",
			       input_char == '&' ? "AND" : "OR"));
			yylloc.end = current_point;
			return input_char == '&' ? T_AND : T_OR;
		}
		unput();
		yylloc.end = current_point;
		
		debug(SLB_DEBCAT_ELEX, 1, ("yylex: %c", c));
		return c;
	}

	/*
	 * Comparison operator
	 */
	if (strchr("<>=!", input_char)) {
		int c = input_char;
		if (input() == '=') {
			switch (c) {
			case '<':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: LE"));
				yylloc.end = current_point;
				return T_LE;
			case '>':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: GE"));
				yylloc.end = current_point;
				return T_GE;
			case '=':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: EQ"));
				yylloc.end = current_point;
				return T_EQ;
			case '!':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: NE"));
				yylloc.end = current_point;
				return T_NE;
			}
		} else {
			unput();
			yylloc.end = current_point;
			switch (c) {
			case '<':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: LT"));
				return T_LT;
			case '>':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: GT"));
				return T_GT;
			case '!':
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: NOT"));
				return T_NOT;
			default:
				debug(SLB_DEBCAT_ELEX, 1, ("yylex: %c", c));
				return c;
			}
		}
	}

	/*
	 * Power or multiplication
	 */
	if (input_char == '*') {
		if (input() == '*') {
			debug(SLB_DEBCAT_ELEX, 1, ("yylex: **"));
			yylloc.end = current_point;
			return T_POW;
		}
		unput();
	}
	
	debug(SLB_DEBCAT_ELEX, 1, ("yylex: %c", input_char));
	yylloc.end = current_point;
	return input_char;
}

int
compile_expression(const char *text, grecs_locus_t *locus,
		   struct slb_expression **pexpr)
{
	int rc;
	
	inbuf = curp = text;
	yyeof = 0;
	yylloc = *locus;
	current_point = yylloc.beg;
	parse_tree = NULL;
	parse_vartab = grecs_symtab_create_default(sizeof(struct slb_varref));

	yydebug = debug_level[SLB_DEBCAT_EGRAM];
	rc = yyparse();

	if (rc == 0) {
		struct slb_expression *expr = grecs_zalloc(sizeof(*expr));
		expr->ex_name = NULL;
		expr->ex_text = grecs_strdup(text);
		if (locus) 
			expr->ex_locus = *locus;
		else
			memset(&expr->ex_locus, 0, sizeof(expr->ex_locus));
		/* FIXME: Optimize parse_tree */
		expr->ex_tree = parse_tree;
		expr->ex_vartab = parse_vartab;
		*pexpr = expr;
	} else {
		grecs_symtab_free(parse_vartab);
		parse_vartab = NULL;
	}
	return rc;
}
			
static struct slb_node *
alloc_node(struct grecs_locus_point beg, struct grecs_locus_point end)
{
	struct slb_node *p = grecs_zalloc(sizeof(*p));
	p->n_locus.beg = beg;
	p->n_locus.end = end;
	p->n_type = node_unknown;
	return p;
}


void *
get_node_state(struct slb_node_state **ps, struct slb_node *node,
	       void *key, size_t size, void (*initfn)(void*))
{
	struct slb_node_state *ns;

	if (*ps) {
		for (ns = *ps; ns; ns = ns->next) {
			if (ns->node == node && ns->key == key &&
			    ns->size == size)
				return ns->data;
		}
	}
	ns = grecs_malloc(sizeof(*ns));
	ns->data = grecs_zalloc(size);
	ns->initfn = initfn;
	if (initfn)
		initfn(ns->data);
	ns->node = node;
	ns->key = key;
	ns->size = size;
	ns->next = *ps;
	*ps = ns;
	return ns->data;
}

void
slb_node_state_init(struct slb_node_state *ns)
{
	for (; ns; ns = ns->next)
		if (ns->initfn)
			ns->initfn(ns->data);
}
		

struct prev_val {
	int init;
	double val;
	time_t ts;
	unsigned long serial;
};

static void
init_d(void *p)
{
	struct prev_val *pv = p;
	pv->init = 0;
}

int
func_d(struct slb_node_state **pstate, struct slb_node *node,
       int nargs, double *args, double *res)
{
	struct prev_val *pv = get_node_state(pstate, node, func_d,
					     sizeof(*pv), init_d);

	if (!pv->init) {
		pv->init = 1;
		*res = 0;
	/* There is no optimizer so far, so the same function can
	   be called several times for the same node.  Make sure it
	   returns the same result.
	   FIXME: Implement optimizer. */
	} else if (pv->serial == slb_loop_serial) {
		*res = pv->val;
		return 0;
	} else
		*res = (*args - pv->val) / (slb_loop_ts - pv->ts);
	pv->val = *args;
	pv->ts = slb_loop_ts;
	pv->serial = slb_loop_serial;

	return 0;
}

int
func_max(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	double x = args[0];
	int i;

	for (i = 1; i < nargs; i++)
		if (args[i] > x)
			x = args[i];
	*res = x;
	return 0;
}

int
func_min(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	double x = args[0];
	int i;

	for (i = 1; i < nargs; i++)
		if (args[i] < x)
			x = args[i];
	*res = x;
	return 0;
}

int
func_avg(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	double x = 0;
	int i;

	for (i = 0; i < nargs; i++)
		x += args[i];
	*res = x / nargs;
	return 0;
}

int
func_log(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	*res = log(*args);
	return 0;
}

int
func_log10(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	*res = log10(*args);
	return 0;
}

int
func_exp(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	*res = exp(*args);
	return 0;
}

int
func_pow(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	*res = pow(args[0], args[1]);
	return 0;
}

int
func_sqrt(struct slb_node_state **pstate, struct slb_node *node,
	  int nargs, double *args, double *res)
{
	*res = sqrt(args[0]);
	return 0;
}

int
func_abs(struct slb_node_state **pstate, struct slb_node *node,
	 int nargs, double *args, double *res)
{
	*res = fabs(args[0]);
	return 0;
}

int
func_ceil(struct slb_node_state **pstate, struct slb_node *node,
	  int nargs, double *args, double *res)
{
	*res = ceil(args[0]);
	return 0;
}

int
func_floor(struct slb_node_state **pstate, struct slb_node *node,
	   int nargs, double *args, double *res)
{
	*res = floor(args[0]);
	return 0;
}

int
func_round(struct slb_node_state **pstate, struct slb_node *node,
	   int nargs, double *args, double *res)
{
	*res = round(args[0]);
	return 0;
}

int
func_trunc(struct slb_node_state **pstate, struct slb_node *node,
	   int nargs, double *args, double *res)
{
	*res = trunc(args[0]);
	return 0;
}


struct function functab[] = {
	{ "d", func_d, 1, 1, FUN_DAEMON_ONLY, 1 },
	{ "max", func_max, 2, SLB_MAX_ARGS },
	{ "min", func_min, 2, SLB_MAX_ARGS },
	{ "avg", func_avg, 2, SLB_MAX_ARGS },
	{ "log", func_log, 1, 1 },
	{ "log10", func_log10, 1, 1 },
	{ "pow", func_pow, 2, 2 },
	{ "sqrt", func_sqrt, 1, 1 },
	{ "abs", func_abs, 1, 1 },
	{ "ceil", func_ceil, 1, 1 },
	{ "floor", func_floor, 1, 1 },
	{ "trunc", func_trunc, 1, 1 },
	{ "round", func_round, 1, 1 },
	{ "exp", func_exp, 1, 1 },
	{ NULL }
};

static struct function *
function_lookup(const char *name, size_t len)
{
	struct function *fp;

	for (fp = functab; fp->name; fp++) {
		if (strlen(fp->name) == len &&
		    memcmp(fp->name, name, len) == 0)
			return fp;
	}
	return NULL;
}


#define EVT_NUMBER  0
#define EVT_BOOLEAN 1

struct eval_result {
	int type;
	union {
		int boolean;
		double number;
	} v;
};

static int
to_boolean(struct eval_result *res)
{
	switch (res->type) {
	case EVT_NUMBER:
		return res->v.number;

	case EVT_BOOLEAN:
		return res->v.boolean;
	}
	abort();
}

static double
to_number(struct eval_result *res)
{
	switch (res->type) {
	case EVT_NUMBER:
		return res->v.number;

	case EVT_BOOLEAN:
		return res->v.boolean;
	}
	abort();
}

static int
evalnode(struct slb_node *node, struct slb_node_state **pstate,
	 struct eval_result *res)
{
	struct eval_result tmp1, tmp2;
	double args[SLB_MAX_ARGS];
	int i;
	struct slb_node *np;
	
	switch (node->n_type) {
	case node_unknown:
		abort();
		
	case node_variable:
		res->type = EVT_NUMBER;
		res->v.number = node->v.n_var->vr_inst->vi_value;
		break;
		
	case node_const:
		res->type = EVT_NUMBER;
		res->v.number = node->v.n_const;
		break;
		
	case node_uminus:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = - to_number(&tmp1);
		break;
		
	case node_add:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = to_number(&tmp1) + to_number(&tmp2);
		break;
		
	case node_sub:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = to_number(&tmp1) + to_number(&tmp2);
		break;

	case node_mul:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = to_number(&tmp1) * to_number(&tmp2);
		break;
		
	case node_div:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = to_number(&tmp1) / to_number(&tmp2);
		break;

	case node_pow:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		res->type = EVT_NUMBER;
		res->v.number = pow(to_number(&tmp1), to_number(&tmp2));
		break;
		
	case node_or:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (!to_boolean(&tmp1)) {
			if (evalnode(node->v.n_arg[1], pstate, &tmp1))
				return 1;
		}
		res->v.boolean = to_boolean(&tmp1);
		break;
		
	case node_and:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (to_boolean(&tmp1)) {
			if (evalnode(node->v.n_arg[1], pstate, &tmp1))
				return 1;
		}
		res->v.boolean = to_boolean(&tmp1);
		break;
		
	case node_not:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		res->v.boolean = !to_boolean(&tmp1);
		break;
		
	case node_eq:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean == tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean =
					tmp1.v.number == tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) == to_number(&tmp2);
		break;
		
	case node_ne:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean != tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean =
					tmp1.v.number != tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) != to_number(&tmp2);
		break;

 	case node_lt:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean < tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean = tmp1.v.number < tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) < to_number(&tmp2);
		break;
		
	case node_le:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean <= tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean =
					tmp1.v.number <= tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) <= to_number(&tmp2);
		break;
		
	case node_gt:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean > tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean =
					tmp1.v.number > tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) > to_number(&tmp2);
		break;
		
	case node_ge:
		res->type = EVT_BOOLEAN;
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (evalnode(node->v.n_arg[1], pstate, &tmp2))
			return 1;
		if (tmp1.type == tmp2.type) {
			switch (tmp1.type) {
			case EVT_BOOLEAN:
				res->v.boolean =
					tmp1.v.boolean >= tmp2.v.boolean;
				break;

			case EVT_NUMBER:
				res->v.boolean =
					tmp1.v.number >= tmp2.v.number;
			}
		} else
			res->v.boolean = to_number(&tmp1) >= to_number(&tmp2);
		break;
		
	case node_cond:
		if (evalnode(node->v.n_arg[0], pstate, &tmp1))
			return 1;
		if (to_boolean(&tmp1)) {
			if (node->v.n_arg[1]) {
				if (evalnode(node->v.n_arg[1], pstate, res))
					return 1;
			} else
				*res = tmp1;
		} else {
			if (evalnode(node->v.n_arg[2], pstate, res))
				return 1;
		}
		break;

	case node_func:
		for (i = 0, np = node->v.n_func.args; np;
		     i++, np = np->n_next) {
			if (evalnode(np, pstate, &tmp1))
				return 1;
			args[i] = to_number(&tmp1);
		}

		if (node->v.n_func.func(pstate, node, node->v.n_func.nargs,
					args, &res->v.number))
			return 1;
		res->type = EVT_NUMBER;
		break;

	case node_subexpr:
		if (evalnode(node->v.n_expr.expr->ex_tree, pstate, res))
			return 1;
		break;

	case node_asgn:
		if (evalnode(node->v.n_asgn.expr, pstate, res))
			return 1;
		node->v.n_asgn.var->vr_inst->vi_value = to_number(res);
		break;

	case node_list:
		for (np = node->n_next; np; np = np->n_next) {
			if (evalnode(np, pstate, res))
				return 1;
		}
	}
	return 0;
}

static int
_print_arg(void *item, void *data)
{
	struct slb_varref *vr = item;

	debug_printf("%s=%g", vr->vr_sym.name, vr->vr_inst->vi_value);
	return 0;
}

int
eval_expression(struct slb_expression *expr, struct slb_node_state **pstate,
		double *pnum)
{
	struct eval_result res;

	if (debug_level[SLB_DEBCAT_EVAL] >= 2) {
		debug_printf("evaluating expression \"%s\"", expr->ex_text);
		debug_printf("input arguments:");
		grecs_symtab_foreach(expr->ex_vartab, _print_arg, NULL);
	}
	
	if (evalnode(expr->ex_tree, pstate, &res)) {
		debug(SLB_DEBCAT_EVAL, 1,
		      ("failure evaluating expression \"%s\"",
		       expr->ex_text));
		return 1;
	}
	*pnum = to_number(&res);
	debug(SLB_DEBCAT_EVAL, 2, ("expression yields %g", *pnum));
	if (expr->ex_eval_count < expr->ex_min_eval) {
		debug(SLB_DEBCAT_EVAL, 1,
		      ("temporary failure evaluating expression \"%s\": "
		       "minimal eval count not reached (%d < %d)",
		       expr->ex_text,
		       expr->ex_eval_count, expr->ex_min_eval));
		++expr->ex_eval_count;
		return 1;
	}
	return 0;
}
	
	
struct slb_node *
copy_node(struct slb_node *node, struct grecs_symtab *vartab)
{
	struct slb_node *new_node, *tail, *np;

	if (!node)
		return NULL;
	
	new_node = alloc_node(node->n_locus.beg, node->n_locus.end);

	new_node->n_type = node->n_type;
	new_node->n_min_eval = node->n_min_eval;
	switch (node->n_type) {
	case node_unknown:
		abort();
		
	case node_variable: {
		int install = 1;
		new_node->v.n_var = varref_lookupz(vartab,
						   node->v.n_var->vr_sym.name,
						   &install);
		break;
	}
		
	case node_uminus:
	case node_not:
		new_node->v.n_arg[0] = copy_node(node->v.n_arg[0], vartab);
		break;
		
	case node_add:
	case node_sub:
	case node_mul:
	case node_div:
	case node_or:
	case node_and:
	case node_eq:
	case node_ne:
 	case node_lt:
	case node_le:
	case node_gt:
	case node_ge:
	case node_pow:
		new_node->v.n_arg[0] = copy_node(node->v.n_arg[0], vartab);
		new_node->v.n_arg[1] = copy_node(node->v.n_arg[1], vartab);
		break;
		
	case node_cond:
		new_node->v.n_arg[0] = copy_node(node->v.n_arg[0], vartab);
		new_node->v.n_arg[1] = copy_node(node->v.n_arg[1], vartab);
		new_node->v.n_arg[2] = copy_node(node->v.n_arg[2], vartab);
		break;

	case node_func:
		new_node->v.n_func.func = node->v.n_func.func;
		new_node->v.n_func.nargs = node->v.n_func.nargs;
		tail = NULL;
		for (np = node->v.n_func.args; np; np = np->n_next) {
			struct slb_node *nnp = copy_node(np, vartab);
			if (tail)
				tail->n_next = nnp;
			else
				new_node->v.n_func.args = nnp;
			tail = nnp;
		}
		break;

	case node_subexpr:
		/* FIXME: copy_expression? */
		memcpy(&new_node->v, &node->v, sizeof(new_node->v));
		break;

	case node_const:
		memcpy(&new_node->v, &node->v, sizeof(new_node->v));
		break;

	case node_asgn: {
		int install = 1;
		new_node->v.n_asgn.var =
			varref_lookupz(vartab,
				       node->v.n_asgn.var->vr_sym.name,
				       &install);
		new_node->v.n_asgn.expr = copy_node(node->v.n_asgn.expr,
						    vartab);
		break;
	}

	case node_list:
		tail = NULL;
		for (np = node->n_next; np; np = np->n_next) {
			struct slb_node *node = copy_node(np, vartab);
			if (!tail)
				new_node->n_next = node;
			else
				tail->n_next = node;
			tail = node;
		}
	}

	return new_node;
}

