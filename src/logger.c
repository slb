/* This file is part of SLB
   Copyright (C) 2011, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"
#include "grecs/locus.h"

int log_to_stderr = -1; /* -1 means autodetect */
int syslog_facility = LOG_DAEMON;
const char *syslog_tag = NULL;
int syslog_include_prio; /* syslog messages include priority */

struct log_trans {
	char *kw;
	int tok;
};

static struct log_trans facility_trans[] = {
	{ "USER",    LOG_USER },   
	{ "DAEMON",  LOG_DAEMON },
	{ "AUTH",    LOG_AUTH },
	{ "AUTHPRIV",LOG_AUTHPRIV },
	{ "MAIL",    LOG_MAIL },
	{ "CRON",    LOG_CRON },
	{ "LOCAL0",  LOG_LOCAL0 },
	{ "LOCAL1",  LOG_LOCAL1 },
	{ "LOCAL2",  LOG_LOCAL2 },
	{ "LOCAL3",  LOG_LOCAL3 },
	{ "LOCAL4",  LOG_LOCAL4 },
	{ "LOCAL5",  LOG_LOCAL5 },
	{ "LOCAL6",  LOG_LOCAL6 },
	{ "LOCAL7",  LOG_LOCAL7 },
	{ NULL }
};

static int
syslog_to_n(struct log_trans *trans, const char *str, int *pint)
{
	int i;

	for (i = 0; trans[i].kw; i++) {
		if (strcasecmp(trans[i].kw, str) == 0) {
			*pint = trans[i].tok;
			return 0;
		}
	}
	return 1;
}

static int
n_to_syslog(struct log_trans *trans, int tok, const char **pstr)
{
	int i;

	for (i = 0; trans[i].kw; i++) {
		if (trans[i].tok == tok) {
			*pstr = trans[i].kw;
			return 0;
		}
	}
	return 1;
}

static struct log_trans prio_trans[] = {
	{ "EMERG", LOG_EMERG },
	{ "ALERT", LOG_ALERT },
	{ "CRIT", LOG_CRIT },
	{ "ERR", LOG_ERR },
	{ "WARNING", LOG_WARNING },
	{ "NOTICE", LOG_NOTICE },
	{ "INFO", LOG_INFO },
	{ "DEBUG", LOG_DEBUG },
	{ NULL }
};

int
string_to_syslog_facility(const char *str, int *pfacility)
{
	return syslog_to_n(facility_trans, str, pfacility);
}

/* Logging */
void
syslog_printer(int prio, grecs_locus_t const *locus, const char *fmt,
	       va_list ap)
{
	static char *fmtbuf;
	static size_t fmtsize;
	char *locstr = NULL;
	char *pfxbuf = NULL;
	const char *p;
	
	if (locus) {
		size_t size = 0;

		if (locus->beg.col == 0)
			grecs_asprintf(&locstr, &size, "%s:%u",
				       locus->beg.file,
				       locus->beg.line);
		else if (strcmp(locus->beg.file, locus->end.file))
			grecs_asprintf(&locstr, &size, "%s:%u.%u-%s:%u.%u",
				       locus->beg.file,
				       locus->beg.line, locus->beg.col,
				       locus->end.file,
				       locus->end.line, locus->end.col);
		else if (locus->beg.line != locus->end.line)
			grecs_asprintf(&locstr, &size, "%s:%u.%u-%u.%u",
				       locus->beg.file,
				       locus->beg.line, locus->beg.col,
				       locus->end.line, locus->end.col);
		else
			grecs_asprintf(&locstr, &size, "%s:%u.%u-%u",
				       locus->beg.file,
				       locus->beg.line, locus->beg.col,
				       locus->end.col);
	}

	if (syslog_include_prio && n_to_syslog(prio_trans, prio, &p) == 0) {
		size_t size = 0;

		if (locstr) {
			grecs_asprintf(&pfxbuf, &size, "%s: [%s]",
				       locstr, p);
			free(locstr);
			locstr = NULL;
		} else
			grecs_asprintf(&pfxbuf, &size, "[%s]",
				       p);
	} else
		pfxbuf = locstr;

	grecs_vasprintf(&fmtbuf, &fmtsize, fmt, ap); 

	if (pfxbuf) {
		syslog(prio, "%s: %s", pfxbuf, fmtbuf);
		free(pfxbuf);
	} else
		syslog(prio, "%s", fmtbuf);
}

void
stderr_printer(int prio, grecs_locus_t const *locus, const char *fmt,
	       va_list ap)
{
	const char *p;
	fprintf (stderr, "%s: ", syslog_tag);

	if (locus) {
		YY_LOCATION_PRINT(stderr, *locus);
		fprintf(stderr, ": ");
	}
	if (n_to_syslog(prio_trans, prio, &p) == 0)
		fprintf(stderr, "[%s] ", p);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

static void (*log_printer)(int prio, grecs_locus_t const *locus,
			   const char *fmt, va_list ap) = stderr_printer;

void
logger_setup()
{
	if (!syslog_tag) {
		syslog_tag = strrchr(program_name, '/');
		if (syslog_tag)
			syslog_tag++;
		else
			syslog_tag = program_name;
	}
	log_printer = log_to_stderr ? stderr_printer : syslog_printer;
	if (log_to_stderr)
		log_printer = stderr_printer;
	else {
		log_printer = syslog_printer;
		openlog(syslog_tag, LOG_PID, syslog_facility);
	}
	grecs_log_to_stderr = log_to_stderr;
}

void
vlogmsg(int prio, const char *fmt, va_list ap)
{
	log_printer(prio, NULL, fmt, ap);
}

void
logmsg(int prio, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	log_printer(prio, NULL, fmt, ap);
	va_end(ap);
}

void
logmsg_at_locus(int prio, grecs_locus_t const *locus, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	log_printer(prio, locus, fmt, ap);
	va_end(ap);
}

void
debug_printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	log_printer(LOG_DEBUG, NULL, fmt, ap);
	va_end(ap);
}
