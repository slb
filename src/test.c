/* This file is part of SLB
   Copyright (C) 2011 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"
#include <wordsplit.h>

static FILE *input;
const char *input_name;
unsigned input_line;
char input_buf[1024];
int input_ready;

static int
islabel(const char *s)
{
	size_t len = strlen(s);

	return len > 0 && s[len-1] == ':';
}
		

static int
input_read_line(void)
{
	if (input_ready)
		input_ready = 0;
	else if (fgets(input_buf, sizeof(input_buf), input)) {
		size_t len = strlen(input_buf);
		if (input_buf[len-1] == '\n')
			input_buf[len-1] = 0;
		input_line++;
	} else
		return 1;
	return 0;
}

enum test_cmd {
	cmd_eof,
	cmd_send,
	cmd_label,
	cmd_error
};

static enum test_cmd
getcmd(void)
{
	if (input_read_line())
		return cmd_eof;
	if (!input_buf[0])
		return cmd_send;
	if (islabel(input_buf)) {
		input_buf[strlen(input_buf)-1] = 0;
		return cmd_label;
	}
	return cmd_error;
}

static void
input_error_recovery(void)
{
	while (input_read_line() == 0)
		if (islabel(input_buf)) {
			input_ready = 1;
			break;
		}
}

static void
input_read_server(struct slb_server *srv)
{
	struct wordsplit ws;
	int wsflags = WRDSF_NOVAR | WRDSF_NOCMD | WRDSF_QUOTE |
		      WRDSF_SQUEEZE_DELIMS;

	if (!srv->pdu)
		srv->pdu = snmp_pdu_create(SNMP_MSG_GET);
	while (1) {
		oid oid[MAX_OID_LEN];
		size_t oidlen;
		
		if (input_read_line())
			break;
		if (!input_buf[0]) {
			input_ready = 1;
			break;
		}
		if (wordsplit(input_buf, &ws, wsflags)) {
			logmsg(LOG_ERR,
			       "%s:%d: cannot split line: %s",
			       input_name, input_line,
			       wordsplit_strerror(&ws));
			continue;
		}

		if (ws.ws_wordc == 1 && islabel(ws.ws_wordv[0])) {
			input_ready = 1;
			break;
		}
		
		ws.ws_flags |= WRDSF_REUSE;

		if (ws.ws_wordc != 3) {
			logmsg(LOG_ERR,
			       "%s:%d: malformed line",
			       input_name, input_line);
			continue;
		}
		
		oidlen = MAX_OID_LEN;
		if (!read_objid(ws.ws_wordv[0], oid, &oidlen)) {
			logmsg(LOG_ERR,
			       "%s:%d: cannot parse oid",
			       input_name, input_line);
			continue;
		}

		if (snmp_add_var(srv->pdu, oid, oidlen,
				 *ws.ws_wordv[1], ws.ws_wordv[2])) {
			logmsg(LOG_CRIT,
			       "%s:%d: cannot add variable: %s",
			       input_name, input_line,
			       snmp_api_errstring(snmp_errno));
			exit(EX_DATAERR);
		}
	}
	wordsplit_free(&ws);
}

static void
test_server_replies()
{
	struct slb_server *srv;
	
	for (srv = srv_head; srv; srv = srv->next) {
		if (srv->flags & SLB_SRV_DISABLED)
			continue;
		
		varinst_unset(srv->varinst);
		srv->flags &= ~SLB_SRV_COMPUTED;
		
		server_process_pdu(srv->pdu ?
				   NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE :
				   NETSNMP_CALLBACK_OP_TIMED_OUT,
				   srv,
				   srv->pdu);
		if (srv->pdu) {
			snmp_free_pdu(srv->pdu);
			srv->pdu = NULL;
		}
	}
	sort();
	slb_fmtout();
	slb_loop_serial++;
	slb_loop_ts += wakeup_interval;
}

int
slb_test(const char *name)
{
	enum test_cmd cmd;
	
	if (!name) {
		input = stdin;
		input_name = "<stdin>";
	} else {
		input = fopen(name, "r");
		if (!input) {
			logmsg(LOG_CRIT, "cannot open input file %s: %s",
			       name, strerror(errno));
			return EX_NOINPUT;
		}
		input_name = name;
	}
	input_line = 0;
	
	while ((cmd = getcmd()) != cmd_eof) {
		struct slb_server *srv;

		switch (cmd) {
		case cmd_label:
			srv = server_lookup(input_buf);
			if (!srv) {
				logmsg(LOG_ERR,
				       "%s:%d: unknown server id",
				       input_name, input_line);
				input_error_recovery();
				continue;
			} else if (srv->flags & SLB_SRV_DISABLED) {
				logmsg(LOG_INFO,
				       "%s:%d: inoring disabled server %s",
				       input_name, input_line, srvid(srv));
				input_error_recovery();
				continue;
			} else {
				input_read_server(srv);
			}
			break;

		case cmd_send:
			test_server_replies();
			break;

		case cmd_error:
			logmsg(LOG_ERR,
			       "%s:%d: unrecognized input",
			       input_name, input_line);
			input_error_recovery();
			continue;

		default:
			break;
		}
	}
	
	fclose(input);
	return EX_OK;
}
