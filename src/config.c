/* This file is part of SLB
   Copyright (C) 2011, 2012, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"
#include "wordsplit.h"

struct grecs_list *mib_file_list;

struct slb_server *srv_head, *srv_tail;
size_t srv_count;
char *default_expression;
int snmp_version = SNMP_VERSION_1;

void
register_server(struct slb_server *srv)
{
	if (srv_tail)
		srv_tail->next = srv;
	else
		srv_head = srv;
	srv_tail = srv;
	srv_count++;
}

struct slb_server *
server_lookup(const char *id)
{
	struct slb_server *srv;

	for (srv = srv_head; srv; srv = srv->next)
		if (strcmp(srv->id, id) == 0)
			break;
	return srv;
}


int
assert_string_value(grecs_locus_t *locus,
		    enum grecs_callback_command cmd,
		    const grecs_value_t *value)
{
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_STRING) {
		grecs_error(locus, 0, _("expected scalar value"));
		return 1;
	}
	return 0;
}

/* FIXME */
#ifndef ngettext
# define ngettext(s,p,n) ((n) == 1 ? (s) : (p))
#endif

int
assert_n_strings(grecs_locus_t *locus,
		 enum grecs_callback_command cmd,
		 const grecs_value_t *value, int minargs, int maxargs)
{
	int i;
	
	if (cmd != grecs_callback_set_value) {
		grecs_error(locus, 0, _("Unexpected block statement"));
		return 1;
	}
	if (!value || value->type != GRECS_TYPE_ARRAY) {
		if (minargs == 1 && value->type == GRECS_TYPE_STRING)
			return 0;
		if (minargs == maxargs)
			grecs_error(locus, 0,
				    ngettext("expected %d argument",
					     "expected %d arguments",
					     minargs),
				    minargs);
		else
			grecs_error(locus, 0,
				    _("expected %d to %d arguments"),
				    minargs, maxargs);
		return 1;
	}

	if (value->v.arg.c < minargs) {
		grecs_error(locus, 0, _("too few arguments"));
		return 1;
	}
	if (value->v.arg.c > maxargs) {
		grecs_error(locus, 0, _("too many arguments"));
		return 1;
	}

	for (i = 0; i < value->v.arg.c; i++) {
		if (value->v.arg.v[i]->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0,
				    _("argument %d is not a string"), i);
			return 1;
		}
	}
	return 0;
}

static int
cb_mib_directory(enum grecs_callback_command cmd,
		 grecs_locus_t *locus,
		 void *varptr,
		 grecs_value_t *value,
		 void *cb_data)
{
	if (assert_string_value(locus, cmd, value))
		return 1;
	add_mibdir(value->v.string);
	return 0;
}

int
cb_define_expression(enum grecs_callback_command cmd,
		     grecs_locus_t *locus,
		     void *varptr,
		     grecs_value_t *value,
		     void *cb_data)
{
	int rc;
	struct slb_expression *expr;

	if (assert_n_strings(locus, cmd, value, 2, 2))
		return 1;

	if (compile_expression(value->v.arg.v[1]->v.string,
			       &value->v.arg.v[1]->locus, &expr))
		return 1;
	expr->ex_name = value->v.arg.v[0]->v.string;
	
	rc = register_expression(expr);
	free(expr);
	return rc;
}

int
cb_server_expression(enum grecs_callback_command cmd,
		     grecs_locus_t *locus,
		     void *varptr,
		     grecs_value_t *value,
		     void *cb_data)
{
	struct slb_expression **pexpr = varptr;

	if (assert_string_value(locus, cmd, value))
		return 1;
	
	return compile_expression(value->v.string, &value->locus, pexpr);
}

int
cb_table(enum grecs_callback_command cmd,
	 grecs_locus_t *locus,
	 void *varptr,
	 grecs_value_t *value,
	 void *cb_data)
{
	struct grecs_symtab *tp = *(struct grecs_symtab **)varptr;
	struct slb_table *tab;
	size_t oidlen;
	oid oid[MAX_OID_LEN];
	int install;
	
	if (assert_n_strings(locus, cmd, value, 2, 2))
		return 1;

	oidlen = MAX_OID_LEN;
	if (!read_objid(value->v.arg.v[1]->v.string, oid, &oidlen)) {
		grecs_error(locus, 0, _("cannot parse oid"));
		return 1;
	}

	install = 1;
	tab = table_lookupz(tp, value->v.arg.v[0]->v.string, &install);
	if (!install) {
		grecs_error(locus, 0,
			    _("redefinition of table %s"), tab->tab_sym.name);
		grecs_symtab_clear(tab->tab_idx);
	} else
		tab->tab_idx =
			grecs_symtab_create_default(sizeof(struct slb_idxnum));
	
	tab->tab_oidlen = oidlen;
	memcpy(tab->tab_oid, oid, sizeof(oid));
	
	return 0;
}

static void
clone_locus(grecs_locus_t *dst, grecs_locus_t *src)
{
	*dst = *src;
	dst->beg.file = grecs_strdup(dst->beg.file);
	dst->end.file = grecs_strdup(dst->end.file);
}

static struct slb_index *
parse_index(char *name, struct slb_server *srv, grecs_locus_t *locus)
{
	char *p;
	size_t len = strlen(name);
	struct slb_index *idx;
	
	if (name[len-1] == ']' && (p = strchr(name, '['))) {
		struct slb_table *tab;
		size_t tlen = p - name;

		tab = table_lookup(srv->tables, name, tlen, NULL);
		if (!tab) 
			grecs_error(locus, 0,
				    _("no such SNMP table: %.*s"),
				    tlen, name);
		else {
			int install = 1;

			idx = index_lookupz(srv->indices, name, &install);
			if (install) {
				size_t ilen = len - tlen - 2;

				idx->idx_table = tab;
				idx->idx_tag = grecs_malloc(ilen + 1);
				memcpy(idx->idx_tag, p + 1, ilen);
				idx->idx_tag[ilen] = 0;
			}
		}
	} else {
		grecs_error(locus, 0, _("invalid index: %s"), name);
	}
	return idx;
}

int
cb_server_variable(enum grecs_callback_command cmd,
		   grecs_locus_t *locus,
		   void *varptr,
		   grecs_value_t *value,
		   void *cb_data)
{
	struct slb_server *srv = varptr;
	struct grecs_symtab *vt = srv->varinst;
	struct slb_varinstance *inst;
	int install;
	char *name;
	
	if (assert_n_strings(locus, cmd, value, 1, 3))
		return 1;
	if (value->type == GRECS_TYPE_STRING)
		name = value->v.string;
	else
		name = value->v.arg.v[0]->v.string;
	install = 1;
	inst = varinst_lookupz(vt, name, &install);
	if (!install)
		grecs_error(locus, 0,
			     _("redefinition of %s"), inst->vi_sym.name);
	clone_locus(&inst->vi_locus, locus);
	if (value->type == GRECS_TYPE_ARRAY && value->v.arg.c >= 2) {
		struct wordsplit ws;
		size_t i, count;

		inst->vi_mib = grecs_strdup(value->v.arg.v[1]->v.string);

		ws.ws_delim = ".";
		if (wordsplit(inst->vi_mib, &ws,
			      WRDSF_NOVAR | WRDSF_NOCMD | WRDSF_DELIM)) {
			grecs_error(locus, 0,
				    _("can't split oid: %s"),
				    wordsplit_strerror(&ws));
			return 1;
		}

		for (i = 0, count = 0; i < ws.ws_wordc; i++)
			if (ws.ws_wordv[i][0] == '$')
				count++;

		if (count) {
			size_t j = 0;
			
			inst->vi_tabrefcnt = count;
			inst->vi_tabref =
				grecs_calloc(count,
					     sizeof(inst->vi_tabref[0]));
			for (i = 0; i < ws.ws_wordc; i++) {
				if (ws.ws_wordv[i][0] == '$') {
					struct slb_index *idx;

					idx = parse_index(ws.ws_wordv[i] + 1,
							  srv, locus);
					if (!idx) {
						wordsplit_free(&ws);
						return 1;
					}
					inst->vi_tabref[j].tr_index = idx;
					inst->vi_tabref[j].tr_idxpos = i;
					j++;
				}
			}
			inst->vi_wordc = ws.ws_wordc;
			inst->vi_wordv = ws.ws_wordv;
			ws.ws_wordc = 0;
			ws.ws_wordv = NULL;
			srv->flags |= SLB_SRV_TAB_REFERENCED;
		}
		wordsplit_free(&ws);

		if (!inst->vi_tabref) {
			inst->vi_oidlen = MAX_OID_LEN;
			if (!read_objid(inst->vi_mib, inst->vi_oid,
					&inst->vi_oidlen)) {
				grecs_error(locus, 0,
					    _("cannot parse oid %s"),
					    inst->vi_mib);
				return 1;
			}
		}
		
		inst->vi_flags = SLV_VIF_SNMP;

		if (value->v.arg.c == 3) {
			if (strcmp(value->v.arg.v[2]->v.string,
				   "increasing") == 0)
				inst->vi_flags |= SLV_VIF_INCR;
			else if (strcmp(value->v.arg.v[2]->v.string,
					"decreasing") == 0)
				inst->vi_flags |= SLV_VIF_DECR;
			else
				grecs_error(locus, 0,
					    _("invalid variable "
					      "description: %s"),
					    value->v.arg.v[2]->v.string);
		}
	} 
	return 0;
}

int
cb_server_constant(enum grecs_callback_command cmd,
		   grecs_locus_t *locus,
		   void *varptr,
		   grecs_value_t *value,
		   void *cb_data)
{
	struct grecs_symtab *vt = *(struct grecs_symtab **)varptr;
	struct slb_varinstance *inst;
	int install;
	char *p;
	
	if (assert_n_strings(locus, cmd, value, 2, 2))
		return 1;

	install = 1;
	inst = varinst_lookupz(vt, value->v.arg.v[0]->v.string, &install);
	if (!install)
		grecs_error(locus, 0,
			     _("redefinition of %s"), inst->vi_sym.name);
	
	inst->vi_value = strtod(value->v.arg.v[1]->v.string, &p);
	if (*p) {
		grecs_error(locus, 0, _("not a valid floating point number"));
		return 1;
	}
	
	inst->vi_flags = SLV_VIF_CONST | SLV_VIF_SET;
	return 0;
}

struct asrtop {
	char *id;
	int opcode;
	int flags;
};

static struct asrtop asrtop[] = {
	{ "eq",       SLB_ASSERT_EQ, 0 },
	{ "eq/i",     SLB_ASSERT_EQ, SLB_ASSERT_ICASE },
	{ "ne",       SLB_ASSERT_EQ, SLB_ASSERT_NEG },
	{ "ne/i",     SLB_ASSERT_EQ, SLB_ASSERT_NEG|SLB_ASSERT_ICASE },
	{ "prefix",   SLB_ASSERT_PREFIX, 0 },
	{ "prefix/i", SLB_ASSERT_PREFIX, SLB_ASSERT_ICASE },
	{ "suffix",   SLB_ASSERT_SUFFIX, 0 },
	{ "suffix/i", SLB_ASSERT_SUFFIX, SLB_ASSERT_ICASE },
	{ "glob",     SLB_ASSERT_GLOB, 0 },
	{ "glob/i",   SLB_ASSERT_GLOB, SLB_ASSERT_ICASE },
	{ "=",        SLB_ASSERT_EQUAL },
	{ "<=",       SLB_ASSERT_LE },
	{ "<",	      SLB_ASSERT_LT },
	{ ">=",       SLB_ASSERT_LT, SLB_ASSERT_NEG },
	{ ">",        SLB_ASSERT_LE, SLB_ASSERT_NEG },
	{ NULL }
};


static struct asrtop *
find_asrtop(char *s)
{
	struct asrtop *op;
	for (op = asrtop; op->id; op++)
		if (strcmp(s, op->id) == 0)
			return op;
	return NULL;
}

static struct slb_assertion *
parse_assert_argv(char **argv, struct grecs_symtab *at, grecs_locus_t *locus)
{
	struct slb_assertion *ap;
	int install;
	int flags = 0;
	int opcode;
	oid oid[MAX_OID_LEN];
	size_t oidlen;
	size_t len;
	char *s;
	struct asrtop *op;
	
	s = argv[1];
	if (*s == '!') {
		++s;
		flags |= SLB_ASSERT_NEG;
	}
			
	op = find_asrtop(s);
	if (!op) {
		grecs_error(locus, 0, _("invalid operation: %s"), s);
		return NULL;
	}
	opcode = op->opcode;
	flags ^= op->flags;

	oidlen = MAX_OID_LEN;
	if (!read_objid(argv[0], oid, &oidlen)) {
		grecs_error(locus, 0, _("cannot parse oid"));
		return NULL;
	}
	
	install = 1;
	ap = assertion_lookup(at, oid, oidlen, &install);
	if (!install)
		grecs_error(locus, 0, _("redefinition of assertion"));

	ap->opcode = opcode;
	ap->flags = flags;
	if (strcmp(argv[2], "-prev") == 0) {
		ap->value = NULL;
		ap->flags |= SLB_ASSERT_PREV;
	} else if (argv[2][0] == '-') {
		ap->value = grecs_strdup(argv[2] + 1);
		ap->vallen = strlen(ap->value);
	} else {
		ap->value = grecs_strdup(argv[2]);
		ap->vallen = strlen(ap->value);
	}
	clone_locus(&ap->locus, locus);
	
	len = strlen(argv[0]) + 1 +
		strlen(argv[1]) + 1 +
		strlen(argv[2]) + 1;
	ap->text = grecs_malloc(len);
	strcpy(ap->text, argv[0]);
	strcat(ap->text, " ");
	strcat(ap->text, argv[1]);
	strcat(ap->text, " ");
	strcat(ap->text, argv[2]);
	return ap;
}

static struct slb_assertion *
parse_assert_string(char *str, struct grecs_symtab *at, grecs_locus_t *locus)
{
	struct slb_assertion *ap;
	struct wordsplit ws;

	if (wordsplit(str, &ws, WRDSF_DEFFLAGS)) {
		grecs_error(locus, 0, "can't split argument: %s",
			    wordsplit_strerror(&ws));
		return NULL;
	}
	if (ws.ws_wordc > 3) {
		grecs_error(locus, 0, "too many arguments to assertion");
		wordsplit_free(&ws);
		return NULL;
	} else if (ws.ws_wordc < 3) {
		grecs_error(locus, 0, "too few arguments to assertion");
		wordsplit_free(&ws);
		return NULL;
	}
	ap = parse_assert_argv(ws.ws_wordv, at, locus);
	wordsplit_free(&ws);
	return ap;
}

static int
cb_server_assert(enum grecs_callback_command cmd,
		 grecs_locus_t *locus,
		 void *varptr,
		 grecs_value_t *value,
		 void *cb_data)
{
	struct slb_server *srv = varptr;
	void **pdata = cb_data;
	struct slb_assertion *ap;
	
	switch (cmd) {
	case grecs_callback_section_begin:
		ap = parse_assert_string(value->v.string, srv->assertions,
					 locus);
		if (!ap)
			return 1;
		*pdata = ap;
		break;

	case grecs_callback_section_end:
		/*ap = *pdata;*/
		break;

	case grecs_callback_set_value:
		if (value->type == GRECS_TYPE_STRING) {
			if (!parse_assert_string(value->v.string,
						 srv->assertions, locus))
				return 1;
		} else {
			char *argv[4];

			if (assert_n_strings(locus, cmd, value, 3, 3))
				return 1;
			
			argv[0] = value->v.arg.v[0]->v.string;
			argv[1] = value->v.arg.v[1]->v.string;
			argv[2] = value->v.arg.v[2]->v.string;
			argv[3] = NULL;
			if (!parse_assert_argv(argv, srv->assertions, locus))
				return 1;
		}
	}
	return 0;
}

static int
cb_assertion_action(enum grecs_callback_command cmd,
		    grecs_locus_t *locus,
		    void *varptr,
		    grecs_value_t *value,
		    void *cb_data)
{
	struct slb_assertion *asrt = varptr;
	if (assert_string_value(locus, cmd, value))
		return 1;
	if (strcmp(value->v.string, "abort") == 0)
		asrt->action = SLB_ASSERT_ABORT;
	else if (strcmp(value->v.string, "warn") == 0)
		asrt->action = SLB_ASSERT_WARN;
	else if (strcmp(value->v.string, "reinit") == 0)
		asrt->action = SLB_ASSERT_REINIT;
	else {
		grecs_error(locus, 0, "unknown action");
		return 1;
	}
	return 0;
}

static struct grecs_keyword server_assert_kw[] = {
	{ "action", N_("{abort|warn|reinit}"),
	  N_("Action to take when the assertion fails"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, 0,
	  cb_assertion_action },
	{ "message", NULL, N_("Message to issue when the assertion fails"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_assertion, text) },
	{ NULL }
};

int
cb_server_macro(enum grecs_callback_command cmd,
		grecs_locus_t *locus,
		void *varptr,
		grecs_value_t *value,
		void *cb_data)
{
	struct grecs_symtab *mt = *(struct grecs_symtab **)varptr;
	struct slb_macro *mac;
	int install;
	
	if (assert_n_strings(locus, cmd, value, 2, 2))
		return 1;

	install = 1;
	mac = macro_lookupz(mt, value->v.arg.v[0]->v.string, &install);
	if (!install)
		grecs_error(locus, 0,
			    _("redefinition of macro %s"), mac->mac_sym.name);
	
	mac->mac_exp = grecs_strdup(value->v.arg.v[1]->v.string);

	return 0;
}

int
cb_server_enable(enum grecs_callback_command cmd,
		 grecs_locus_t *locus,
		 void *varptr,
		 grecs_value_t *value,
		 void *cb_data)
{
	int *flags = varptr;
	int enable;
	
	if (assert_string_value(locus, cmd, value))
		return 1;
	grecs_string_convert(&enable, grecs_type_bool, value->v.string,
			     locus);
	if (!enable)
		*flags |= SLB_SRV_DISABLED;
	return 0;
}

static void
set_snmpver(int *verptr, const char *str, grecs_locus_t *locus)
{
	if (strcmp(str, "1") == 0)
		*verptr = SNMP_VERSION_1;
	else if (strcmp(str, "2c") == 0)
		*verptr = SNMP_VERSION_2c;
	else if (strcmp(str, "3") == 0)
		*verptr = SNMP_VERSION_3;
	else
		grecs_error(locus, 0, _("unknown SNMP version"));
}

int
cb_server_snmpver(enum grecs_callback_command cmd,
		  grecs_locus_t *locus,
		  void *varptr,
		  grecs_value_t *value,
		  void *cb_data)
{
	if (assert_string_value(locus, cmd, value))
		return 1;

	set_snmpver(varptr, value->v.string, locus);
	return 0;
}

int
cb_snmp_v3_auth_proto(enum grecs_callback_command cmd,
		      grecs_locus_t *locus,
		      void *varptr,
		      grecs_value_t *value,
		      void *cb_data)
{
	struct slb_key *kp = varptr;
	
	if (assert_string_value(locus, cmd, value))
		return 1;
	if (!strcasecmp(value->v.string, "MD5")) {
		kp->proto = usmHMACMD5AuthProtocol;
		kp->proto_len = USM_AUTH_PROTO_MD5_LEN;
	} else if (!strcasecmp(value->v.string, "SHA")) {
		kp->proto = usmHMACSHA1AuthProtocol;
		kp->proto_len = USM_AUTH_PROTO_SHA_LEN;
	} else
		grecs_error(locus, 0,
			    _("unknown SNMPv3 authentication protocol"));
	
	return 0;
}

int
cb_snmp_v3_priv_proto(enum grecs_callback_command cmd,
		      grecs_locus_t *locus,
		      void *varptr,
		      grecs_value_t *value,
		      void *cb_data)
{
	struct slb_key *kp = varptr;
	
	if (assert_string_value(locus, cmd, value))
		return 1;
	if (!strcasecmp(value->v.string, "DES")) {
		kp->proto = usmDESPrivProtocol;
		kp->proto_len = USM_PRIV_PROTO_DES_LEN;
	} else if (!strcasecmp(value->v.string, "AES")) {
		kp->proto = usmAESPrivProtocol;
		kp->proto_len = USM_PRIV_PROTO_AES_LEN;
	} else
		grecs_error(locus, 0,
			    _("unknown SNMPv3 privacy protocol"));
	
	return 0;
}

int
cb_snmp_v3_security_level(enum grecs_callback_command cmd,
			  grecs_locus_t *locus,
			  void *varptr,
			  grecs_value_t *value,
			  void *cb_data)
{
	const char *str = value->v.string;
	int *levelp = varptr;
	
	if (assert_string_value(locus, cmd, value))
		return 1;
	if (!strcasecmp(str, "noAuthNoPriv") ||
	    !strcmp(str, "1") ||
	    !strcasecmp(str, "noauth") ||
	    !strcasecmp(str, "nanp")) {
		*levelp = SNMP_SEC_LEVEL_NOAUTH;
	} else if (!strcasecmp(str, "authNoPriv") ||
		   !strcmp(str, "2") ||
		   !strcasecmp(str, "auth") ||
		   !strcasecmp(str, "anp")) {
		*levelp = SNMP_SEC_LEVEL_AUTHNOPRIV;
	} else if (!strcasecmp(str, "authPriv") ||
		   !strcmp(str, "3") ||
		   !strcasecmp(str, "priv") ||
		   !strcasecmp(str, "ap")) {
		*levelp = SNMP_SEC_LEVEL_AUTHPRIV;
	} else
		grecs_error(locus, 0, "invalid security level");
	return 0;
}

static struct grecs_keyword snmp_v3_kw[] = {
	{ "user", NULL, N_("Set security name"),
	  grecs_type_string, GRECS_DFLT, NULL,
	  offsetof(struct slb_snmp_v3, user) },
	{ "auth-proto", "arg: SHA|MD5", N_("Set authentication protocol"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_snmp_v3, auth),
	  cb_snmp_v3_auth_proto },
	{ "auth-passphrase", NULL, N_("Set authentication passphrase"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, auth.passphrase) },
	{ "priv-proto", "arg: DES|AES", N_("Set privacy protocol"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_snmp_v3, priv),
	  cb_snmp_v3_priv_proto },
	{ "priv-passphrase", NULL, N_("Set privacy passphrase"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, priv.passphrase) },
	{ "context", NULL, N_("Set context name"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, context_name) },
	{ "secure-engine-id", NULL, N_("set security engine ID"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, security_engine_id) },
	{ "security-level", NULL,
	  N_("Set security level: noAuthNoPriv (1, noauth, nanp), "
	     "authNoPriv (2, auth, anp), "
	     "or authPriv (3, priv, ap)"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_snmp_v3, security_level),
	  cb_snmp_v3_security_level },
	{ "engine-boots", NULL, N_("Set initial engine boots value"),
	  grecs_type_uint, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, engine_boots) },
	{ "engine-time", NULL, N_("Set initial engine time value"),
	  grecs_type_uint, GRECS_DFLT,
	  NULL, offsetof(struct slb_snmp_v3, engine_time) },
	{ NULL }
};

static int
snmp_v3_key_setup(struct slb_key *kp, grecs_locus_t *locus)
{
	if (kp->passphrase) {
		if (!kp->proto) {
			kp->proto = usmHMACMD5AuthProtocol;
			kp->proto_len = USM_AUTH_PROTO_MD5_LEN;
		}
		kp->key_len = sizeof(kp->key);
		if (generate_Ku(kp->proto, kp->proto_len,
				(u_char*) kp->passphrase,
				strlen(kp->passphrase),
				kp->key,
				&kp->key_len) != SNMPERR_SUCCESS) {
			grecs_error(locus, 0,
				    "error generating a key (Ku) from "
				    "the supplied authentication pass "
				    "phrase");
			return 1;
		}
		memset(kp->passphrase, 0, strlen(kp->passphrase));
	}
	return 0;
}

static int
parse_engine_id(char *input, grecs_locus_t *locus,
		 u_char **output, size_t *outlen)
{
	size_t ebuf_len = 32, eout_len = 0;
	u_char *ebuf = (u_char *)grecs_malloc(ebuf_len);

	if (!snmp_hex_to_binary(&ebuf, &ebuf_len, &eout_len, 1, input)) {
		grecs_error(locus, 0, "bad engine ID value");
		free(ebuf);
		return 1;
	}
	if (eout_len < 5 || eout_len > 32) {
		grecs_error(locus, 0, "invalid engine ID value");
		free(ebuf);
		return 1;
	}
	*output = ebuf;
	*outlen = eout_len;
	return 0;
}

static int
cb_snmp_v3(enum grecs_callback_command cmd,
	   grecs_locus_t *locus,
	   void *varptr,
	   grecs_value_t *value,
	   void *cb_data)
{
	struct slb_server *srv = varptr;
	void **pdata = cb_data;

	switch (cmd) {
	case grecs_callback_section_begin:
		*pdata = &srv->snmp_v3;
		break;

	case grecs_callback_section_end:
		if (snmp_v3_key_setup(&srv->snmp_v3.auth, locus)
		    || snmp_v3_key_setup(&srv->snmp_v3.priv, locus))
			srv->flags |= SLB_SRV_DISABLED;
		if (srv->snmp_v3.priv.proto && !srv->snmp_v3.priv.key_len &&
		    srv->snmp_v3.auth.key_len) {
			memcpy(srv->snmp_v3.priv.key,
			       srv->snmp_v3.auth.key,
			       srv->snmp_v3.auth.key_len);
			srv->snmp_v3.priv.key_len = srv->snmp_v3.auth.key_len;
		}
		
		if (srv->snmp_v3.security_engine_id &&
		    parse_engine_id(srv->snmp_v3.security_engine_id, locus,
				    &srv->snmp_v3.security_engine_id,
				    &srv->snmp_v3.security_engine_id_len))
			srv->flags |= SLB_SRV_DISABLED;
		if (srv->snmp_v3.context_engine_id &&
		    parse_engine_id(srv->snmp_v3.context_engine_id, locus,
				    &srv->snmp_v3.context_engine_id,
				    &srv->snmp_v3.context_engine_id_len))
			srv->flags |= SLB_SRV_DISABLED;
		break;

	case grecs_callback_set_value:
		grecs_error(locus, 0, _("invalid use of block statement"));
	}
	return 0;
}

static struct grecs_keyword server_kw[] = {
	{ "enable", NULL, N_("Enable or disable monitoring of this server"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_server, flags),
	  cb_server_enable },
	{ "version", "arg: 1|2c|3", N_("Set SNMP version to use"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_server, snmpver),
	  cb_server_snmpver },
	{ "host", NULL, N_("Hostname or IP address"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_server, peer), NULL },
	{ "timeout", NULL, N_("Session timeout"),
	  grecs_type_time, GRECS_DFLT,
	  NULL, offsetof(struct slb_server, timeout), NULL },
	{ "retries", NULL, N_("Number of retries before timeout"),
	  grecs_type_uint, GRECS_DFLT,
	  NULL, offsetof(struct slb_server, retries), NULL },
	{ "community", NULL, N_("Community string"),
	  grecs_type_string, GRECS_DFLT,
	  NULL, offsetof(struct slb_server, community),
	  NULL },
	{ "snmpv3", NULL, N_("SNMPv3 parameters"),
	  grecs_type_section, GRECS_DFLT,
	  NULL, 0,
	  cb_snmp_v3, NULL, snmp_v3_kw },
	{ "expression", NULL, N_("Evaluate expression"),
	  grecs_type_string, GRECS_AGGR,
	  NULL, offsetof(struct slb_server, expr),
	  cb_server_expression },
	{ "table", N_("<name: string> <base-oid: string>"),
	  N_("Define SNMP table"),
	  grecs_type_string, GRECS_MULT,
	  NULL, offsetof(struct slb_server, tables),
	  cb_table },
	{ "variable", N_("<name: string> <oid: string>"),
	  N_("Define SNMP variable"),
	  grecs_type_string, GRECS_MULT,
	  NULL, 0,
	  cb_server_variable },
	{ "constant", N_("<name: string> <value: string>"),
	  N_("Define a constant"),
	  grecs_type_string, GRECS_MULT,
	  NULL, offsetof(struct slb_server, varinst),
	  cb_server_constant },
	{ "macro", N_("<name: string> <expansion: string>"),
	  N_("Define a macro"),
	  grecs_type_string, GRECS_MULT,
	  NULL, offsetof(struct slb_server, macros),
	  cb_server_macro },
	{ "assert", N_("<arg: <oid: string> [!]<opcode>[/i] <value: string>>"),
	  N_("Ensure that <oid> matches the given <value> as per <opcode>.\n"
	     "Valid opcodes are:\n"
	     "Arithmetical operations: =, <, <=, >, >=\n"
	     "String operations:\n"
	     " eq          string equality\n"
	     " ne          string inequality\n"
	     " prefix      oid value must begin with <value>\n"
	     " suffix      oid value must end with <value>\n"
	     " glob        <value> is a glob(7) pattern that oid value must match\n"
	     "Each of these can be suffixed with \"/i\" to request case-insensitive\n"
	     "comparison.\n"
	     "\n"
	     "A \"!\" in front of opcode reverts its meaning.\n\n"
	     "The block part is optional.  If omitted, the default action is \"abort\""),
	  grecs_type_section, GRECS_MULT,
	  NULL, 0,
	  cb_server_assert, NULL, server_assert_kw },
	{ NULL }
};

static struct slb_server *
slb_server_new(const char *id)
{
	struct slb_server *srv = grecs_zalloc(sizeof(*srv));
	srv->id = grecs_strdup(id);
	srv->snmpver = snmp_version;
	srv->varinst = grecs_symtab_create_default(sizeof(struct slb_varinstance));
	srv->macros = grecs_symtab_create_default(sizeof(struct slb_macro));
	srv->tables = grecs_symtab_create_default(sizeof(struct slb_table));
	srv->indices = grecs_symtab_create_default(sizeof(struct slb_index));
	srv->assertions = assertion_symtab_create();
	return srv;
}

static int
cb_server(enum grecs_callback_command cmd,
	  grecs_locus_t *locus,
	  void *varptr,
	  grecs_value_t *value,
	  void *cb_data)
{
	struct slb_server *srv;
	void **pdata = cb_data;

	switch (cmd) {
	case grecs_callback_section_begin:
		if (!value || value->type != GRECS_TYPE_STRING) {
			grecs_error(locus, 0, _("tag must be a string"));
			return 0;
		}
		srv = slb_server_new(value->v.string);
		srv->locus = *locus;
		*pdata = srv;
		break;

	case grecs_callback_section_end:
		srv = *pdata;
		if (!srv->peer) {
			grecs_error(locus, 0,
				    _("server name not defined"));
			return 1;
		}
		register_server(srv);
		*pdata = NULL;
		break;

	case grecs_callback_set_value:
		grecs_error(locus, 0, _("invalid use of block statement"));
	}
	return 0;
}


static int
cb_syslog_facility(enum grecs_callback_command cmd,
		   grecs_locus_t *locus,
		   void *varptr,
		   grecs_value_t *value,
		   void *cb_data)
{
	if (assert_string_value(locus, cmd, value))
		return 1;

	if (string_to_syslog_facility(value->v.string, varptr))
		grecs_error(locus, 0, _("Unknown syslog facility `%s'"),
			    value->v.string);
	return 0;
}

static struct grecs_keyword syslog_kw[] = {
	{ "facility",
	  N_("name"),
	  N_("Set syslog facility. Arg is one of the following: user, daemon, "
	     "auth, authpriv, mail, cron, local0 through local7 (case-insensitive), "
	     "or a facility number."),
	  grecs_type_string, GRECS_AGGR,
	  &syslog_facility, 0, cb_syslog_facility },
	{ "tag", N_("string"), N_("Tag syslog messages with this string"),
	  grecs_type_string, GRECS_DFLT, &syslog_tag },
	{ "print-priority", N_("arg"),
	  N_("Prefix each message with its priority"),
	  grecs_type_bool, GRECS_DFLT, &syslog_include_prio },
	{ NULL },
};


static struct grecs_keyword slb_kw[] = {
	{ "standalone", NULL, N_("Enable standalone mode"),
	  grecs_type_bool, GRECS_DFLT, &standalone_mode },
	{ "foreground", NULL, N_("Start in foreground even in daemon mode"),
	  grecs_type_bool, GRECS_DFLT, &foreground },
	{ "pidfile", N_("file"), N_("Set pid file name"),
	  grecs_type_string, GRECS_DFLT|GRECS_CONST, &pidfile },
#if 0	
	/* These are reserved for future use.  The '' quotes are used to
	   pacify make check-docs in /dir */
	{ ''user'', N_("name"), N_("Run with UID and GID of this user"),
	  grecs_type_string, GRECS_DFLT, NULL, 0, cb_user },
	{ ''group'', NULL, N_("Retain these supplementary groups"),
	  grecs_type_string, GRECS_LIST|GRECS_AGGR, NULL, 0, cb_supp_groups },
#endif

	{ "wakeup", NULL, N_("Wake up each <arg> seconds"),
	  grecs_type_time, GRECS_DFLT, &wakeup_interval },
	
	{ "mib-directory", N_("name"), N_("Search for MIBs in this directory"),
	  grecs_type_string,  GRECS_DFLT, NULL, 0, cb_mib_directory },
	{ "add-mib", N_("file"), N_("Read MIBs from file"),
	  grecs_type_string, GRECS_LIST, &mib_file_list },

	{ "snmp-version", "arg: 1|2c|3", N_("Set default SNMP version to use"),
	  grecs_type_string, GRECS_DFLT, NULL, 0, NULL },
	
	{ "expression",
	  N_("<name: string> <expr: text>"),
	  N_("define a named expression"),
	  grecs_type_string, GRECS_MULT, NULL, 0, cb_define_expression },

	{ "default-expression", NULL,
	  N_("Set the name of the default expression"),
	  grecs_type_string, GRECS_DFLT, &default_expression },
	  
	{ "server", N_("id: string"), N_("Define a server"),
	  grecs_type_section, GRECS_DFLT, NULL, 0, cb_server, NULL, server_kw },

	{ "syslog", NULL, N_("Configure syslog logging"),
	  grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL, syslog_kw },

	{ "tail", NULL,
	  N_("Output at most <arg> records from the sorted list"),
	  grecs_type_size, GRECS_DFLT, &output_tail_count },
	{ "head", NULL,
	  N_("Output at most <arg> records from the sorted list"),
	  grecs_type_size, GRECS_DFLT, &output_head_count },
	{ "output-file", NULL,
	  N_("Name of the output file or command"),
	  grecs_type_string, GRECS_DFLT|GRECS_CONST, &output_file },
	{ "output-format", NULL,
	  N_("format string for output records"),
	  grecs_type_string, GRECS_DFLT|GRECS_CONST, &output_format },
	{ "begin-output-message", NULL,
	  N_("string emitted before formatting output records"),
	  grecs_type_string, GRECS_DFLT, &begin_output_message },
	{ "end-output-message", NULL,
	  N_("string emitted after formatting output records"),
	  grecs_type_string, GRECS_DFLT, &end_output_message },
	{ "suppress-output", NULL,
	  N_("suppress first <arg> outputs"),
	  grecs_type_size, GRECS_DFLT, &output_suppress_count },
	{ NULL }
};

void
config_help()
{
	static char docstring[] =
		N_("Configuration file structure for SLB.\n"
		   "For more information, use `info slb configuration'.");
	grecs_print_docstring(docstring, 0, stdout);
	grecs_print_statement_array(slb_kw, 1, 0, stdout);
}


static void
grecs_print_diag(grecs_locus_t const *locus, int err, int errcode,
		 const char *msg)
{
	logmsg_at_locus(err ? LOG_ERR : LOG_WARNING, locus, "%s", msg);
}

void
config_init()
{
	grecs_include_path_setup(DEFAULT_VERSION_INCLUDE_DIR,
				 DEFAULT_INCLUDE_DIR, NULL);
	grecs_preprocessor = DEFAULT_PREPROCESSOR;
	grecs_log_to_stderr = 1;
	grecs_parser_options = GRECS_OPTION_ADJUST_STRING_LOCATIONS;
	grecs_print_diag_fun = grecs_print_diag;
}


struct config_finish_hook_entry
{
	struct config_finish_hook_entry *next;
	config_finish_hook_t fun;
	void *data;
};

static struct config_finish_hook_entry *cfh_head, *cfh_tail;
	
void
add_config_finish_hook(config_finish_hook_t fun, void *data)
{
	struct config_finish_hook_entry *ent = grecs_malloc(sizeof(*ent));
	ent->next = NULL;
	ent->fun = fun;
	ent->data = data;
	if (cfh_tail)
		cfh_tail->next = ent;
	else
		cfh_head = ent;
	cfh_tail = ent;
}

static int
run_config_finish_hooks()
{
	struct config_finish_hook_entry *p;
	int rc = 0;
	
	for (p = cfh_head; p; p = p->next)
		rc |= p->fun(p->data);
	return rc;
}

void
config_finish(struct grecs_node *tree)
{
	struct grecs_node *node;
	struct slb_server *srv;
	struct slb_expression *defexpr = NULL;

	grecs_tree_reduce(tree, slb_kw, GRECS_AGGR);
	if (debug_level[SLB_DEBCAT_CONF]) {
		grecs_print_node(tree, GRECS_NODE_FLAG_DEFAULT, stderr);
		fputc('\n', stdout);
	}
	
	node = grecs_find_node(tree, ".snmp-version");
	if (node)
		set_snmpver(&snmp_version, node->v.value->v.string,
			    &node->locus);
	
	if (grecs_tree_process(tree, slb_kw))
		exit(EX_CONFIG);
	
	init_mib();

	/* Load requested MIBs */
	if (mib_file_list) {
		struct grecs_list_entry *ep;
		
		for (ep = mib_file_list->head; ep; ep = ep->next)
			read_mib((const char*)ep->data);
	}

	collect_output_refs();

	if (default_expression) {
		defexpr = expression_lookupz(default_expression);
		if (!defexpr) {
			grecs_error(NULL, 0,
				    _("default-expression refers to "
				      "undefined expression"));
			exit(EX_CONFIG);
		}
	}

	for (srv = srv_head; srv; srv = srv->next) {
		if (!srv->expr) {
			if (defexpr)
				srv->expr = copy_expression(defexpr, NULL);
			else {
				grecs_error(&srv->locus, 0,
					    _("server expression not defined"));
				continue;
			}
		}
		if (fixup_expression(srv->expr, srv->varinst))
			srv->flags |= SLB_SRV_DISABLED;
		else {
			report_unbound_output_refs(srv);
			srv->oidtab = create_oidtab(srv->varinst);
		}
	}

	if (grecs_error_count || run_config_finish_hooks())
		exit(EX_CONFIG);
}	
