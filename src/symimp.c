/* This file is part of SLB
   Copyright (C) 2011, 2012, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"

struct grecs_syment *
_lookup(struct grecs_symtab *vit, const char *name, size_t len, int *install,
	char *what)
{
	struct grecs_syment key;
	struct grecs_syment *ent;

	key.name = grecs_malloc(len + 1);
	memcpy(key.name, name, len);
	key.name[len] = 0;
	
	ent = grecs_symtab_lookup_or_install(vit, &key, install);
	free(key.name);
	if (!ent) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("cannot define %s: %s"),
			       what, grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return ent;
}

struct grecs_syment *
_lookupz(struct grecs_symtab *vit, const char *name, int *install, char *what)
{
	struct grecs_syment key;
	struct grecs_syment *ent;

	key.name = (char*) name;
	ent = grecs_symtab_lookup_or_install(vit, &key, install);
	if (!ent) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("cannot define %s: %s"),
			       what, grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return ent;
}

struct slb_varinstance *
varinst_lookup(struct grecs_symtab *vit, const char *name, size_t len,
	       int *install)
{
	return (struct slb_varinstance *)
		_lookup(vit, name, len, install, "variable instance");
}


struct slb_varinstance *
varinst_lookupz(struct grecs_symtab *vit, const char *name, int *install)
{
	return (struct slb_varinstance *)
		_lookupz(vit, name, install, "variable instance");
}

static int
_varinst_unset(void *sym, void *data)
{
	struct slb_varinstance *vp = sym;
	if (vp->vi_flags & SLV_VIF_SNMP) 
		vp->vi_flags &= ~SLV_VIF_SET;
	else
		vp->vi_flags |= SLV_VIF_SET;
	return 0;
}
	
void
varinst_unset(struct grecs_symtab *vit)
{
	grecs_symtab_foreach(vit, _varinst_unset, NULL);
}

static int
_varinst_clear(void *sym, void *data)
{
	struct slb_varinstance *vp = sym;
	if (vp->vi_flags & SLV_VIF_SNMP) {
		vp->vi_flags &= ~SLV_VIF_SET;
		vp->vi_value = 0;
	} else
		vp->vi_flags |= SLV_VIF_SET;
	return 0;
}
	
void
varinst_clear(struct grecs_symtab *vit)
{
	grecs_symtab_foreach(vit, _varinst_clear, NULL);
}

static int
_varinst_allset(void *sym, void *data)
{
	struct slb_varinstance *vp = sym;
	if ((vp->vi_flags & SLV_VIF_USED) &&
	    !(vp->vi_flags & SLV_VIF_SET)) {
		*(int*)data = 0;
		return 1;
	}
	return 0;
}
	
int
varinst_allset(struct grecs_symtab *vit)
{
	int rc = 1;
	grecs_symtab_foreach(vit, _varinst_allset, &rc);
	return rc;
}	


struct slb_varref *
varref_lookup(struct grecs_symtab *vit, const char *name, size_t len,
	      int *install)
{
	return (struct slb_varref *)
		_lookup(vit, name, len, install, "variable");
}

struct slb_varref *
varref_lookupz(struct grecs_symtab *vit, const char *name, int *install)
{
	return (struct slb_varref *)
		_lookupz(vit, name, install, "variable");
}

struct slb_macro *
macro_lookup(struct grecs_symtab *mt, const char *name, size_t len,
	     int *install)
{
	return (struct slb_macro *)
		_lookup(mt, name, len, install, "macro");
}

struct slb_macro *
macro_lookupz(struct grecs_symtab *mt, const char *name, int *install)
{
	return (struct slb_macro *)
		_lookupz(mt, name, install, "macro");
}

struct slb_table *
table_lookup(struct grecs_symtab *tab, const char *name, size_t len,
	     int *install)
{
	return (struct slb_table *)
		_lookup(tab, name, len, install, "table");
}

struct slb_table *
table_lookupz(struct grecs_symtab *tab, const char *name, int *install)
{
	return (struct slb_table *)
		_lookupz(tab, name, install, "table");
}

struct slb_index *
index_lookup(struct grecs_symtab *tab, const char *name, size_t len,
	     int *install)
{
	return (struct slb_index *)
		_lookup(tab, name, len, install, "index");
}

struct slb_index *
index_lookupz(struct grecs_symtab *tab, const char *name, int *install)
{
	return (struct slb_index *)
		_lookupz(tab, name, install, "index");
}

struct slb_idxnum *
idxnum_lookup(struct grecs_symtab *tab, const char *name, size_t len,
	      int *install)
{
	return (struct slb_idxnum  *)
		_lookup(tab, name, len, install, "idxnum");
}

struct slb_idxnum *
idxnum_lookupz(struct grecs_symtab *tab, const char *name, int *install)
{
	return (struct slb_idxnum  *) _lookupz(tab, name, install, "indnum");
}
