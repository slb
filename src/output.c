/* This file is part of SLB
   Copyright (C) 2011, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"

size_t output_tail_count;
size_t output_head_count;
char *output_file = "-";
size_t output_suppress_count;

#define PID_CMD 0
#define PID_ERR 1
#define PID_OUT 2
#define _PID_MAX 3

struct procent
{
	const char *descr;
	pid_t pid;
	int status;
};

struct procent proctab[_PID_MAX] = {
	{ "output command", -1, 0 },
	{ "stderr redirector", -1, 0 },
	{ "stdout redirector", -1, 0 }
};

char *output_format = "%i %w\n";
char *begin_output_message;
char *end_output_message;

FILE *outfile;
struct grecs_symtab *outreftab;

void
slb_add_outfd(fd_set *fdset)
{
	if (outfile)
		FD_SET(fileno(outfile), fdset);
}

void
slb_set_output_defaults()
{
	output_file = "-";
}

/* Spec.               |   Replaced with
   --------------------+------------------------------------------------
   %[0][w][.p]i        |   server id
   %[0][w][.p]h        |   server hostname
   %[0][w][.p]w        |   weight
   %[0][w][.p]{var}    |   value of the variable 'var'
   %[0][w][.p]{@expr}  |   result of evaluating expression expr
   %[0][w][.p](macro)  |   expansion of the 'macro'.
   %%                  |   %
   --------------------+------------------------------------------------
*/
static void
format_srv(struct slb_server *srv,
	   int (*driver)(struct slb_server *, void *,
			 int, const char *, const char *, size_t),
	   void *data)
{
	char *fmt = output_format;
	char *fbuf = grecs_malloc(strlen(output_format) + 2);
	char *p;
	
	while (fmt) {
		size_t len = strcspn(fmt, "%\\");
		if (len > 0) {
			driver(srv, data, 0, NULL, fmt, len);
			fmt += len;
		}
		if (!*fmt)
			break;
		if (*fmt == '%') {
			int code;
			int fn = 0;
			int rc;
			
			fbuf[fn++] = *fmt++;

			if (*fmt == '%') {
				driver(srv, data, 0, NULL, fmt, 1);
				fmt++;
				continue;
			}

			while (*fmt && strchr("- 0", *fmt))
				fbuf[fn++] = *fmt++;

			while (isdigit(*fmt))
				fbuf[fn++] = *fmt++;
			if (*fmt == '.') {
				fbuf[fn++] = *fmt++;
				while (isdigit(*fmt))
					fbuf[fn++] = *fmt++;
			}
			
			switch (code = *fmt++) {
			case 'i':
			case 'h':
				fbuf[fn++] = 's';
				fbuf[fn] = 0;
				rc = driver(srv, data, code, fbuf, NULL, 0);
				break;

			case 'w':
				fbuf[fn++] = 'f';
				fbuf[fn] = 0;
				rc = driver(srv, data, code, fbuf, NULL, 0);
				break;

			case '(':
				p = fmt;
				for (; *fmt && *fmt != ')'; fmt++)
					;
				if (*fmt != ')') {
					fbuf[fn] = 0;
					driver(srv, data, 0, fbuf, "(", 1);
					fmt = p;
				} else {
					fbuf[fn++] = 's';
					fbuf[fn] = 0;
					rc = driver(srv, data,
						    code, fbuf, p, fmt - p);
					if (rc == 0)
						fmt++;
					else {
						fbuf[fn-1] = 0;
						driver(srv, data, 0,
						       fbuf, "(", 1);
						fmt = p;
					}
				}
				break;
				
			case '{':
				p = fmt;
				for (; *fmt && *fmt != '}'; fmt++)
					;
				if (*fmt != '}') {
					fbuf[fn] = 0;
					driver(srv, data, 0, fbuf, NULL, 0);
					fmt = p;
				} else {
					fbuf[fn++] = 'f';
					fbuf[fn] = 0;
					rc = driver(srv, data,
						    code, fbuf, p, fmt - p);
					if (rc == 0)
						fmt++;
					else {
						fbuf[fn-1] = 0;
						driver(srv, data, 0,
						       fbuf, "{", 1);
						fmt = p;
					}
				}
				break;

			default:
				fbuf[fn] = 0;
				driver(srv, data, 0, fbuf, fmt - 1, 1);
				logmsg(LOG_WARNING,
				       "unknown format specification in "
				       "format: %s%c",
				       fbuf, fmt[-1]);
				break;
			}

		}
	}
	free(fbuf);	
}

static int
_ref_var(void *sym, void *data)
{
	struct slb_varref *ref = sym;
	int install = 1;
	
	varref_lookupz(outreftab, ref->vr_sym.name, &install);
	return 0;
}
	
static int
ref_drv(struct slb_server *srv, void *data,
	   int code, const char *fbuf, const char *s, size_t slen)
{
	int install = 1;
	
	if (code != '{')
		return 0;
	if (s[0] == '@') {
		struct slb_expression *expr =
			expression_lookup(s + 1, slen - 1);
		if (!expr) {
			logmsg(LOG_CRIT, "output format refers to undefined "
			       "expression %.*s", (int)(slen - 1), s + 1);
			exit(EX_CONFIG);
		}
		grecs_symtab_foreach(expr->ex_vartab, _ref_var, NULL);
		if (expr->ex_min_eval > output_suppress_count)
			output_suppress_count = expr->ex_min_eval;
		symtab_register_expression(&srv->exprtab, expr);
	} else
		varref_lookup(outreftab, s, slen, &install);
	return 0;
}

void
collect_output_refs()
{
	struct slb_server *srv;
	
	outreftab = grecs_symtab_create_default(sizeof(struct slb_varref));
	for (srv = srv_head; srv; srv = srv->next)
		format_srv(srv, ref_drv, outreftab);
}

int
bind_output_refname(struct slb_varinstance *vinst)
{
	struct slb_varref *ref = varref_lookupz(outreftab,
						vinst->vi_sym.name, NULL);
	if (!ref)
		return 0;
	ref->vr_inst = vinst;
	vinst->vi_flags |= SLV_VIF_USED;
	return 1;
}

static int
_unref(void *sym, void *data)
{
	struct slb_varref *ref = sym;
	ref->vr_inst = NULL;
	return 0;
}

void
undo_output_refs()
{
	grecs_symtab_foreach(outreftab, _unref, NULL);
}

static int
_report_unbound_refs(void *sym, void *data)
{
	struct slb_varref *ref = sym;
	struct slb_server *srv = data;
	if (!ref->vr_inst)
		grecs_error(&srv->locus, 0,
			    "%%{%s} referenced in output-format is "
			    "not defined in server %s",
			    ref->vr_sym.name, srvid(srv));
	return 0;
}

void
report_unbound_output_refs(struct slb_server *srv)
{
	grecs_symtab_foreach(outreftab, _report_unbound_refs, srv);
}


static int
output_drv(struct slb_server *srv, void *data,
	   int code, const char *fbuf, const char *s, size_t slen)
{
	FILE *fp = data;
	
	switch (code) {
	case 0:
		if (fbuf)
			fprintf(fp, "%s", fbuf);
		if (slen)
			fwrite(s, slen, 1, fp);
		break;

	case 'i':
		fprintf(fp, fbuf, srv->id);
		break;

	case 'h':
		fprintf(fp, fbuf, srv->peer);
		break;

	case 'w':
		fprintf(fp, fbuf, srv->weight);
		break;

	case '(': {
		struct slb_macro *mac;
					
		mac = macro_lookup(srv->macros, s, slen, NULL);
		if (!mac) {
			logmsg(LOG_WARNING,
			       "undefined macro in format: %.*s",
			       (int) slen, s);
			return 1;
		} else {
			fprintf(fp, fbuf, mac->mac_exp);
		}
		break;
	}

	case '{':
		if (s[0] == '@') {
			double res;
			struct slb_expression *expr =
				symtab_expression_lookup(srv->exprtab,
							 s + 1, slen - 1);
			if (!expr)
				return 1;
			bind_expression(expr, srv->varinst);
			debug(SLB_DEBCAT_OUTPUT, 3,
			      ("%s: evaluating expression: \"%s\"",
			       srvid(srv), expr->ex_name));
			if (eval_expression(expr, &srv->state, &res) == 0) {
				debug(SLB_DEBCAT_OUTPUT, 3,
				      ("%s: expression yields %g",
				       srvid(srv), res));
				fprintf(fp, fbuf, res);
			} else
				return 1;
		} else {
			struct slb_varinstance *vi;
		
			vi = varinst_lookup(srv->varinst, s, slen, NULL);
			if (!vi) {
				logmsg(LOG_WARNING,
				       "unknown variable in format: %.*s",
				       (int) slen, s);
				return 1;
			} else
				fprintf(fp, fbuf, vi->vi_value);
		}
		break;
		
	default:
		abort();
	}
	return 0;
}

static size_t
format_output(FILE *fp, size_t i, size_t count)
{
	if (count > srvtab_count)
		count = srvtab_count;
	for (; i < count; i++)
		format_srv(srvtab[i], output_drv, fp);
	return count;
}

void
slb_fmtout()
{
	size_t n = 0;
	FILE *fp;
	
	if (!srvtab_count) {
		logmsg(LOG_WARNING, "nothing to output");
		return;
	}

	slb_check_output();

	if (output_suppress_count) {
		--output_suppress_count;
		debug(SLB_DEBCAT_OUTPUT, 1, ("output suppressed"));
		fp = fopen("/dev/null", "w");
		if (!fp)
			return;
	} else
		fp = outfile;
	
	if (begin_output_message)
		fprintf(fp, "%s", begin_output_message);
	if (output_head_count || output_tail_count) {
		if (output_head_count)
			n = format_output(fp, 0, output_head_count);
		if (output_tail_count) {
			size_t first = (srvtab_count < output_tail_count) ?
				       0 : (srvtab_count - output_tail_count);
			if (first < n)
				first = n;
			format_output(fp, first, srvtab_count);
		}
	} else
		format_output(fp, 0, srvtab_count);
	if (end_output_message)
		fprintf(fp, "%s", end_output_message);
	fflush(fp);
	if (fp != outfile)
		fclose(fp);
}

#if defined HAVE_SYSCONF && defined _SC_OPEN_MAX
# define getmaxfd() sysconf(_SC_OPEN_MAX)
#elif defined (HAVE_GETDTABLESIZE)
# define getmaxfd() getdtablesize()
#else
# define getmaxfd() 256
#endif

void
close_fds(fd_set *fdset)
{
	int i;
	
	for (i = getmaxfd(); i >= 0; i--) {
		if (fdset && FD_ISSET(i, fdset))
			continue;
		close(i);
	}
}

int
open_redirector(const char *tag, int prio, int pidn)
{
	int p[2];
	FILE *fp;
	char buf[1024];
	pid_t pid;
	fd_set fdset;
	
	pipe(p);
	switch (pid = fork()) {
	case 0:
		/* Redirector process */
		FD_ZERO(&fdset);
		FD_SET(p[0], &fdset);
		if (log_to_stderr)
			FD_SET(2, &fdset);
		close_fds(&fdset);
		set_signals(SIG_DFL, slb_sigtab, slb_sigcount);
      
		close(p[1]);
		fp = fdopen(p[0], "r");
		if (fp == NULL)
			_exit(1);
		syslog_tag = tag;
		logger_setup();
		while (fgets(buf, sizeof(buf), fp)) {
			int cont;
			char *p = buf + strlen(buf) - 1;
			if (*p == '\n') {
				*p = 0;
				cont = 0;
			} else
				cont = 1;
			if (cont)
				logmsg(prio, "%s\\", buf);
			else
				logmsg(prio, "%s", buf);
		}
		_exit(0);
      
	case -1:
		logmsg(LOG_CRIT,
		       "cannot run redirector `%s': fork failed: %s",
		       tag, strerror(errno));
		return -1;

	default:
		debug(SLB_DEBCAT_OUTPUT, 1,
		      ("redirector for %s started, pid=%lu",
		       tag, (unsigned long) pid));
		proctab[pidn].pid = pid;
		close(p[0]);
		return p[1];
	}
}

static void
sigchld(int sig)
{
	pid_t pid;
	int status;
	
	if ((pid = waitpid((pid_t)-1, &status, WNOHANG)) != -1) {
		int i;
		for (i = 0; i < _PID_MAX; i++)
			if (proctab[i].pid == pid) {
				proctab[i].pid = -1;
				proctab[i].status = status;
			}
	}
}
	
static void
waitforproc(int pidn)
{
	int i;
	
	for (i = 0; i < 10; i++) {
		if (proctab[pidn].pid == -1) {
			int status = proctab[pidn].status;
			if (WIFEXITED(status))
				debug(SLB_DEBCAT_OUTPUT, 1,
				      ("%s finished with status %d",
				       proctab[pidn].descr,
				       WEXITSTATUS(status)));
			else if (WIFSIGNALED(status))
				debug(SLB_DEBCAT_OUTPUT, 1,
				      ("%s terminated on signal %d",
				       proctab[pidn].descr,
				       WTERMSIG(status)));
			else
				debug(SLB_DEBCAT_OUTPUT, 1,
				      ("%s terminated",
					  proctab[pidn].descr));
			return;
		}
		sleep(1);
	}
	logmsg(LOG_NOTICE, "%s did not finish; killing it",
	       proctab[pidn].descr);
	
	kill(proctab[pidn].pid, SIGKILL);
}

static void
open_pipe(const char *cmd)
{
	int err, out;
	int p[2];
	pid_t pid;
	fd_set fdset;
	int sigtab[] = { SIGCHLD };
	char *argv[4];
	
	while (*cmd && isspace(*cmd))
		cmd++;

	if (*cmd == 0) {
		logmsg(LOG_CRIT, "empty output command");
		exit(EX_CONFIG);
	}

	set_signals(sigchld, sigtab, sizeof(sigtab) / sizeof(sigtab[0]));
	
	pipe(p);
	out = open_redirector("stdout", LOG_INFO, PID_OUT);
	err = open_redirector("stderr", LOG_ERR, PID_ERR);

	switch (pid = fork()) {
		/* The child branch.  */
	case 0:
		set_signals(SIG_DFL, slb_sigtab, slb_sigcount);

		dup2(p[0], 0);
		dup2(out, 1);
		dup2(err, 2);

		FD_ZERO(&fdset);
		FD_SET(0, &fdset);
		FD_SET(1, &fdset);
		FD_SET(2, &fdset);
		close_fds(&fdset);

		argv[0] = "/bin/sh";
		argv[1] = "-c";
		argv[2] = (char*) cmd;
		argv[3] = NULL;
		execv(argv[0], argv);
		_exit(127);

	case -1:
		logmsg(LOG_CRIT,
		       "cannot run `%s': fork failed: %s",
			cmd, strerror(errno));
		exit(EX_UNAVAILABLE);

	default:
		proctab[PID_CMD].pid = pid;
		debug(SLB_DEBCAT_OUTPUT, 1,
		      ("%s started, pid=%lu", cmd, (unsigned long) pid));
	}
	outfile = fdopen(p[1], "w");
	close(p[0]);
}

static void
open_file(const char *file)
{
	outfile = fopen(file, "w");
	if (!outfile) {
		logmsg(LOG_CRIT, "cannot open output file %s: %s",
		       file, strerror(errno));
		exit(EX_UNAVAILABLE);
	}
}

static void
file_closefn()
{
	fclose(outfile);
}

static void
proc_closefn()
{
	fclose(outfile);
	waitforproc(PID_CMD);
	if (proctab[PID_ERR].pid != -1) {
		kill(proctab[PID_ERR].pid, SIGTERM);
		waitforproc(PID_ERR);
	}
	if (proctab[PID_OUT].pid != -1) {
		kill(proctab[PID_OUT].pid, SIGTERM);
		waitforproc(PID_OUT);
	}
}

static void
proc_checkfn()
{
	if (proctab[PID_CMD].pid == -1) {
		fclose(outfile);
		outfile = NULL;
		waitforproc(PID_CMD);
		if (proctab[PID_ERR].pid != -1) {
			kill(proctab[PID_ERR].pid, SIGTERM);
			waitforproc(PID_ERR);
		}
		if (proctab[PID_OUT].pid != -1) {
			kill(proctab[PID_OUT].pid, SIGTERM);
			waitforproc(PID_OUT);
		}

		open_pipe(output_file + 1);
	}
}

void (*closeoutfn)(void);
void (*checkoutfn)(void);

void
slb_open_output()
{
	if (strcmp(output_file, "-") == 0)
		outfile = stdout;
	else if (*output_file == '|') {
		open_pipe(output_file + 1);
		closeoutfn = proc_closefn;
		checkoutfn = proc_checkfn;
	} else {
		open_file(output_file);
		closeoutfn = file_closefn;
	}
}

void
slb_close_output()
{
	if (closeoutfn)
		closeoutfn();
}

void
slb_check_output()
{
	if (checkoutfn)
		checkoutfn();
}
