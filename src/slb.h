/* This file is part of SLB
   Copyright (C) 2011, 2012 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sysexits.h>
#include <syslog.h>
#include <errno.h>
#include <signal.h>

#include <stdio.h>
#include <stddef.h>
#include <ctype.h>

#include "grecs.h"

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>

extern const char *program_name;

extern int slb_sigtab[];
extern size_t slb_sigcount;
extern unsigned long slb_loop_serial;
extern time_t slb_loop_ts;

void set_signals(void (*handler) (int signo), int *sigv, int sigc);

void config_help(void);
void config_init(void);
void config_finish(struct grecs_node *tree);

typedef int (*config_finish_hook_t)(void *);
void add_config_finish_hook(config_finish_hook_t fun, void *data);

extern int standalone_mode;
extern int foreground;
extern char *pidfile;
extern time_t wakeup_interval;


#define	SLB_DEBCAT_MAIN    0
#define	SLB_DEBCAT_EVAL    1
#define	SLB_DEBCAT_EGRAM   2
#define	SLB_DEBCAT_ELEX    3
#define	SLB_DEBCAT_SNMP    4
#define	SLB_DEBCAT_OUTPUT  5
#define SLB_DEBCAT_CFGRAM  6
#define SLB_DEBCAT_CFLEX   7
#define SLB_DEBCAT_CONF    8
#define _SLB_DEBCAT_MAX    9

extern int debug_level[];

#define debug(cat, lev, s)				\
	do {						\
		if (debug_level[cat] >= (lev))		\
		       debug_printf s;			\
	} while(0)


enum node_type {
	node_unknown,
	node_variable,
	node_const,
	node_uminus,
	node_add,
	node_sub,
	node_mul,
	node_div,
	node_or,
	node_and,
	node_not,
	node_eq,
	node_ne,
	node_lt,
	node_le,
	node_gt,
	node_ge,
	node_cond,
	node_func,
	node_subexpr,
	node_pow,
	node_asgn,
	node_list
};

/* Maximum number of arguments to a built-in function */
#define SLB_MAX_ARGS 256

struct slb_node;
struct slb_expression;
struct slb_node_state;

typedef int (*slb_func_t) (struct slb_node_state **pstate,
			   struct slb_node *node,
			   int nargs, double *args,
			   double *res);

#define FUN_DAEMON_ONLY 0x01 /* Function runs only in daemon mode */

struct function {
	const char *name;   /* Function name */
	slb_func_t func;    /* Implementation */
	int minargs;        /* Min. number of arguments */
	int maxargs;        /* Max. number of arguments */
	int flags;          /* Flags */
	int min_eval;       /* Min. number of evaluations needed to
			       compute the result */
};

struct slb_node {
	grecs_locus_t n_locus;
	enum node_type n_type;    /* Node type */
	struct slb_node *n_next;  /* Next node, if part of a list */
	int n_min_eval; /* Min. number of evaluations needed to
			   obtain the result */
	union {
		struct slb_node *n_arg[3];   /* arguments for arith. nodes */ 
		double n_const;              /* for node_const */ 
		struct slb_varref *n_var;    /* for node_variable */
		struct {
			slb_func_t func;         /* Function handling func. */ 
			struct slb_node *args;   /* List of actual arguments */
			size_t nargs;            /* Number of args */
		} n_func;                    /* for node_func */ 
		struct {
			char *name;
			struct slb_expression *expr;
		} n_expr;                    /* for node_subexpr */
		struct {
			struct slb_varref *var;
			struct slb_node *expr;
		} n_asgn;
	} v;
};

struct slb_node_list {
	struct slb_node *head;
	struct slb_node *tail;
	size_t count;
};


struct slb_varref {
	struct grecs_syment vr_sym;
	struct slb_varinstance *vr_inst;
};

struct slb_tabref {
	struct slb_index *tr_index;
	size_t tr_idxpos;
};

#define SLV_VIF_CONST   0x01
#define SLV_VIF_USED    0x02
#define SLV_VIF_SET     0x04
#define SLV_VIF_SNMP    0x08
#define SLV_VIF_INCR    0x10 /* Monotonic increasing */
#define SLV_VIF_DECR    0x20 /* Monotonic decreasing */

struct slb_varinstance {
	struct grecs_syment vi_sym;
	char *vi_mib;
	oid vi_oid[MAX_OID_LEN];
	size_t vi_oidlen;
	double vi_value;
	int vi_flags;
	char *vi_extra;
	struct slb_tabref *vi_tabref;
	size_t vi_tabrefcnt;
	grecs_locus_t vi_locus;
	size_t vi_wordc;
	char **vi_wordv;
};

struct slb_expression {
	const char *ex_name;      /* Expression name (if named) */
	const char *ex_text;      /* Actual expression text */
	int ex_fixed;             /* Is the expression fixed up */
	grecs_locus_t ex_locus;   /* Location in the config file */
	struct grecs_symtab *ex_vartab; /* Table of variables used in this
					   expr. */
	struct slb_node *ex_tree; /* Compiled parse tree */
#define ex_min_eval ex_tree->n_min_eval
	int ex_eval_count;        /* Number of evaluations so far.
				     Always <= ex_min_eval. */
};

struct slb_macro {
	struct grecs_syment mac_sym;
	const char *mac_exp;
};


struct slb_varinstance *varinst_lookup(struct grecs_symtab *vit,
				       const char *name, size_t len,
				       int *install);
struct slb_varinstance *varinst_lookupz(struct grecs_symtab *vit,
					const char *name, int *install);
struct slb_varref *varref_lookup(struct grecs_symtab *vit,
				 const char *name, size_t len,
				 int *install);
struct slb_varref *varref_lookupz(struct grecs_symtab *vit,
				  const char *name, int *install);
struct slb_macro *macro_lookup(struct grecs_symtab *mt,
			       const char *name, size_t len,
			       int *install);
struct slb_macro *macro_lookupz(struct grecs_symtab *mt,
				const char *name, int *install);

void varinst_unset(struct grecs_symtab *vit);
void varinst_clear(struct grecs_symtab *vit);
int varinst_allset(struct grecs_symtab *vit);

struct grecs_symtab *create_oidtab(struct grecs_symtab *vit);
int oidtab_install(struct grecs_symtab *oidtab, struct slb_varinstance *vinst);
int oidtab_delete(struct grecs_symtab *oidtab, struct slb_varinstance *vinst);
struct slb_varinstance *oidtab_lookup(struct grecs_symtab *oidtab,
				      oid *name, size_t name_length);

struct slb_table {
	struct grecs_syment tab_sym;
	oid tab_oid[MAX_OID_LEN];
	size_t tab_oidlen;
	struct grecs_symtab *tab_idx;
};

struct slb_table *table_lookup(struct grecs_symtab *tab, const char *name,
			       size_t len, int *install);
struct slb_table *table_lookupz(struct grecs_symtab *tab, const char *name,
				int *install);

struct slb_idxnum {
	struct grecs_syment idxnum_sym;
	unsigned char *idxnum_strval;
	size_t idxnum_strsiz;
};

struct slb_idxnum *idxnum_lookup(struct grecs_symtab *tab,
				 const char *name, size_t len,
				 int *install);
struct slb_idxnum *idxnum_lookupz(struct grecs_symtab *tab, const char *name,
				  int *install);


struct slb_index {
	struct grecs_syment idx_sym;
	char *idx_tag;
	struct slb_table *idx_table;
};

struct slb_index *index_lookup(struct grecs_symtab *tab, const char *name,
			       size_t len, int *install);
struct slb_index *index_lookupz(struct grecs_symtab *tab, const char *name,
				int *install);

int symtab_register_expression(struct grecs_symtab **pexptab,
			       struct slb_expression *expr);
struct slb_expression *symtab_expression_lookupz(struct grecs_symtab *exptab,
						 const char *name);
struct slb_expression *symtab_expression_lookup(struct grecs_symtab *exptab,
						const char *name, size_t len);

int register_expression(struct slb_expression *expr);
struct slb_expression *expression_lookupz(const char *name);
struct slb_expression *expression_lookup(const char *name, size_t len);
struct slb_expression *copy_expression(struct slb_expression *src,
				       struct grecs_symtab *vt);
int fixup_expression(struct slb_expression *expr, struct grecs_symtab *vit);
void bind_expression(struct slb_expression *expr, struct grecs_symtab *vit);

int compile_expression(const char *text, grecs_locus_t *locus,
		       struct slb_expression **pexpr);
int eval_expression(struct slb_expression *expr,
		    struct slb_node_state **pstate,
		    double *pnum);
struct slb_node *copy_node(struct slb_node *node, struct grecs_symtab *vartab);

void slb_node_state_init(struct slb_node_state *ns);

struct slb_format_cache {
	unsigned char *cache_buffer;
	size_t cache_bufsize;
};

#define SLB_FORMAT_CACHE_NULL { NULL, 0 }

void slb_format_cache_init(struct slb_format_cache *cache);
char *slb_format_oid_value(oid *oid, size_t len, struct variable_list *vp,
			   struct slb_format_cache *cache);



#define SLB_ASSERT_NEG     0x01
#define SLB_ASSERT_ICASE   0x02
#define SLB_ASSERT_PREV    0x04

/* Opcodes */
#define SLB_ASSERT_EQ      1
#define SLB_ASSERT_PREFIX  2
#define SLB_ASSERT_SUFFIX  3
#define SLB_ASSERT_GLOB    4
#define SLB_ASSERT_EQUAL   5
#define SLB_ASSERT_LT      6
#define SLB_ASSERT_LE      7

/* Actions */
#define SLB_ASSERT_ABORT   0
#define SLB_ASSERT_WARN    1
#define SLB_ASSERT_REINIT  2

struct slb_assertion {
	oid name[MAX_OID_LEN];
	size_t length;
	int opcode;
	int flags;
	int action;
	char *value;
	size_t vallen;
	char *text;
	grecs_locus_t locus;
	struct slb_format_cache cache;
};

struct grecs_symtab *assertion_symtab_create(void);
struct slb_assertion *assertion_lookup(struct grecs_symtab *at, oid *name,
				       size_t namelen, int *install);

struct slb_key {
	oid *proto;
	size_t proto_len;
	char *passphrase;
	u_char key[USM_AUTH_KU_LEN];
	size_t key_len;
};

struct slb_snmp_v3 {
	char *user;          /* Security name */ 
	struct slb_key auth; /* Authentication protocol & key */
	struct slb_key priv; /* Privacy protocol & key */
	char *context_name;  /* Context name */
	u_char *security_engine_id; /* Security ID */
	size_t security_engine_id_len;
	u_char *context_engine_id;  /* Context ID */
	size_t context_engine_id_len;
	int security_level;
	u_int engine_boots; /* initial engine boots for remote engine */
	u_int engine_time;  /* initial engine time for remote engine */
};

#define SLB_SRV_DISABLED       0x01     /* Server is disabled */
#define SLB_SRV_COMPUTED       0x02     /* Expression has been computed */    
#define SLB_SRV_TAB_REFERENCED 0x04     /* Variables refer to SNMP tables */
#define SLB_SRV_TAB_RESOLVED   0x08     /* Table references resolved */
#define SLB_SRV_DEBUG_OUTPUT   0x10     /* Suppress further debugging output
					   about received NEXT responses. */

struct slb_server {
	char *id;                  /* Server ID */
	grecs_locus_t locus;       /* Location in the config file */
	int snmpver;               /* SNMP version */
	char *peer;                /* Server IP or host name */
	int port;                  /* Port it is listening on (not used yet) */
	time_t timeout;            /* SNMP timeout */
	unsigned retries;          /* Number of SNMP retries */
	char *community;           /* SNMP v1/v2c community */
	struct slb_snmp_v3 snmp_v3;/* SNMP v3 data */
	struct grecs_symtab *assertions; /* Assertions for this server */
	struct grecs_symtab *varinst;    /* Variable instances */
	struct grecs_symtab *oidtab;     /* A reverse index to the entries from
				            varinst */
	struct grecs_symtab *macros;     /* Defined macros */
	struct grecs_symtab *tables;     /* SNMP tables */
	struct grecs_symtab *indices;    /* Indices to SNMP tables */
	struct slb_node_state *state; /* Preserved node states for expression
					 evaluator */
	struct grecs_symtab *exprtab; /* Table of expressions used by
					 output driver of this server */
	struct slb_expression *expr; /* Expression returning the server
					load "weight" */

	struct snmp_session *sess; /* SNMP session */        
	struct snmp_pdu *pdu;      /* PDU used in test mode and when handling
				      GETNEXT requests. */
	double weight; /* Last computed server weight (relative load) */
	int flags;                 /* Server flags */
	struct slb_server *next;   /* Next server in the list */
};

extern struct slb_server *srv_head, *srv_tail;
extern size_t srv_count;
extern struct slb_server **srvtab;
extern size_t srvtab_count;

void snmploop(void);
void sort(void);
char *srvid(struct slb_server *srv);
struct slb_server *server_lookup(const char *id);
void server_process_pdu(int operation, struct slb_server *srv,
			struct snmp_pdu *pdu);


extern int log_to_stderr;
extern int syslog_facility;
extern const char *syslog_tag;
extern int syslog_include_prio;

void logger_setup(void);
void logmsg(int prio, const char *fmt, ...) SLB_PRINTFLIKE(2,3);
void logmsg_at_locus(int prio, grecs_locus_t const *locus,
		     const char *fmt, ...) SLB_PRINTFLIKE(3,4);
void debug_printf(const char *fmt, ...) SLB_PRINTFLIKE(1,2);

int string_to_syslog_facility(const char *str, int *pfacility);


extern size_t output_tail_count;
extern size_t output_head_count;
extern size_t output_suppress_count;
extern char *output_file;
extern char *output_format;
extern char *output_file_option;
extern char *begin_output_message;
extern char *end_output_message;

void slb_set_output_defaults(void);
void slb_open_output(void);
void slb_close_output(void);
void slb_check_output(void);

void slb_fmtout(void);

void collect_output_refs(void);
int bind_output_refname(struct slb_varinstance *vinst);
void undo_output_refs(void);
void report_unbound_output_refs(struct slb_server *srv);

void close_fds(fd_set *fdset);

int slb_test(const char *name);

int wildmatch(char *expr, char *name, int icase);


