/* This file is part of SLB
   Copyright (C) 2011, 2012, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"

unsigned long slb_loop_serial;
time_t slb_loop_ts;
struct slb_server **srvtab;
size_t srvtab_count;

static size_t active_count;

char *
srvid(struct slb_server *srv)
{
	return srv->id ? srv->id : srv->peer;
}

void
slb_format_cache_init(struct slb_format_cache *cache)
{
	memset(cache, 0, sizeof(*cache));
}
	
char *
slb_format_oid_value(oid *oid, size_t len, struct variable_list *vp,
		     struct slb_format_cache *cache)
{
	struct slb_format_cache tcache;
	size_t out_len = 0;
	int rc;

	if (!cache) {
		slb_format_cache_init(&tcache);
		cache = &tcache;
	}
	
	if (cache->cache_bufsize == 0) {
		/* Initial allocation */
		cache->cache_bufsize = 16;
		cache->cache_buffer = grecs_malloc(cache->cache_bufsize);
	}
	if (!sprint_realloc_value(&cache->cache_buffer, &cache->cache_bufsize,
				  &out_len, 1, oid, len, vp))
		return NULL;
	if (vp->type == ASN_OCTET_STR &&
	    cache->cache_buffer[0] == '"' &&
	    cache->cache_buffer[out_len-1] == '"') {
		memmove(cache->cache_buffer, cache->cache_buffer + 1,
			out_len-2);
		cache->cache_buffer[out_len-2] = 0;
	}
	return (char*) cache->cache_buffer;
}

static int
assrt_clear_prev(void *sym, void *data)
{
	struct slb_assertion *ap = sym;
	if (ap->flags & SLB_ASSERT_PREV) {
		free(ap->value);
		ap->value = NULL;
	}
	return 0;
}

static int
table_clear(void *sym, void *data)
{
	struct slb_table *tab = sym;
	grecs_symtab_clear(tab->tab_idx);
	return 0;
}

/* Re-initialize server state */
static int
slb_server_init(struct slb_server *srv, int clrflag)
{
	slb_node_state_init(srv->state);
	srv->expr->ex_eval_count = 0;
	grecs_symtab_foreach(srv->assertions, assrt_clear_prev, NULL);
	if (clrflag) {
		grecs_symtab_foreach(srv->tables, table_clear, NULL);
		srv->flags &= ~clrflag;
	}
}

#define E_VARINST_OK 0
#define E_VARINST_UNSUPPORTED 1
#define E_VARINST_OVERFLOW 2

static int
varinst_assign(struct slb_varinstance *inst, struct variable_list *var,
	       struct slb_server *srv)
{
	int rc = E_VARINST_OK;
	double val;
	
	switch (var->type) {
	case ASN_INTEGER:
		val = *var->val.integer;
		break;
			
	case ASN_UINTEGER:
	case ASN_GAUGE:
	case ASN_COUNTER:
		val = (unsigned) *var->val.integer;
		break;
		
	case ASN_COUNTER64:
		val = (double) var->val.counter64->high * 4294967296.0 +
			             var->val.counter64->low;
		break;
		
#ifdef NETSNMP_WITH_OPAQUE_SPECIAL_TYPES
	case ASN_OPAQUE_FLOAT:
		val = *var->val.floatVal;
		break;
		
	case ASN_OPAQUE_DOUBLE:
		val = *var->val.doubleVal;
		break;
#endif
		
	default:
		logmsg(LOG_ERR,
		       "server %s: variable %s has unsupported type (%d)",
		       srvid(srv), inst->vi_sym.name,
		       var->type);
		return E_VARINST_UNSUPPORTED;
	}

	if (((inst->vi_flags & SLV_VIF_INCR) && val < inst->vi_value)
	    || ((inst->vi_flags & SLV_VIF_DECR) && val > inst->vi_value)) {
		logmsg(LOG_NOTICE,
		       "server %s: variable %s overflow (old=%f, new=%f)",
		       srvid(srv), inst->vi_sym.name,
		       inst->vi_value, val);
		rc = E_VARINST_OVERFLOW;
	} else
		inst->vi_value = val;

	return rc;
}

static int
assertion_test(struct slb_server *srv, struct slb_assertion *ap,
	       struct variable_list *vp)
{
	size_t out_len = 0;
	int rc;
	char *buf;

	buf = slb_format_oid_value(ap->name, ap->length, vp, &ap->cache);
	if (!buf)
		return 1;

	if (!ap->value) {
		rc = 1;
	} else {
		switch (ap->opcode) {
		case SLB_ASSERT_EQ:
			rc = ((ap->flags & SLB_ASSERT_ICASE) ?
			      strcasecmp : strcmp)
				(ap->value, buf) == 0;
			break;
		case SLB_ASSERT_PREFIX:
			rc = strlen(buf) >= ap->vallen &&
				((ap->flags & SLB_ASSERT_ICASE) ?
				 strncasecmp : strncmp)
				(buf, ap->value, ap->vallen) == 0;
			break;
		case SLB_ASSERT_SUFFIX:
			rc = strlen(buf) >= ap->vallen &&
				((ap->flags & SLB_ASSERT_ICASE) ?
				 strncasecmp : strncmp)
				(buf + strlen(buf) - ap->vallen,
				 ap->value, ap->vallen) == 0;
			break;
		case SLB_ASSERT_GLOB:
			rc = wildmatch(ap->value, buf,
				       ap->flags & SLB_ASSERT_ICASE) == 0;
			break;
		case SLB_ASSERT_EQUAL:
			/* FIXME */
			rc = strtoul(buf, NULL, 10) ==
				strtoul(ap->value, NULL, 10);
			break;
		case SLB_ASSERT_LT:
			rc = strtoul(buf, NULL, 10) <
				strtoul(ap->value, NULL, 10);
			break;
		case SLB_ASSERT_LE:
			rc = strtoul(buf, NULL, 10) <=
				strtoul(ap->value, NULL, 10);
		}
		if (ap->flags & SLB_ASSERT_NEG)
			rc = !rc;
	}
	
	if (ap->flags & SLB_ASSERT_PREV) {
		free(ap->value);
		ap->value = grecs_strdup(buf);
		ap->vallen = strlen(ap->value);
	}
	return rc;
}

static void
process_result(struct slb_server *srv, struct snmp_pdu *pdu)
{
	struct variable_list *vp;
	
	debug(SLB_DEBCAT_SNMP, 2, ("reply from %s", srvid(srv)));
	for (vp = pdu->variables; vp; vp = vp->next_variable) {
		struct slb_assertion *ap;
		struct slb_varinstance *inst;
		char buf[1024];

		ap = assertion_lookup(srv->assertions, vp->name,
				      vp->name_length, NULL);
		if (ap && !assertion_test(srv, ap, vp)) {
			switch (ap->action) {
			case SLB_ASSERT_ABORT:
				logmsg(LOG_NOTICE,
				       "%s:%d: %s: assertion \"%s\" failed, "
				       "got %s",
				       ap->locus.beg.file, ap->locus.beg.line,
				       srvid(srv),
				       ap->text,
				       ap->cache.cache_buffer);
				return;

			case SLB_ASSERT_WARN:
				logmsg(LOG_NOTICE,
				       "%s:%d: %s: warning: "
				       "assertion \"%s\" failed, "
				       "got %s",
				       ap->locus.beg.file, ap->locus.beg.line,
				       srvid(srv),
				       ap->text,
				       ap->cache.cache_buffer);
				break;

			case SLB_ASSERT_REINIT:
				logmsg(LOG_NOTICE,
				       "%s:%d: %s: assertion \"%s\" failed "
				       "(got %s); re-initializing server",
				       ap->locus.beg.file, ap->locus.beg.line,
				       srvid(srv),
				       ap->text,
				       ap->cache.cache_buffer);
				slb_server_init(srv, SLB_SRV_TAB_RESOLVED);
				return;
			}
		}
		
		inst = oidtab_lookup(srv->oidtab, vp->name, vp->name_length);
		if (!inst) {
			if (ap)
				continue;
			snprint_variable(buf, sizeof(buf),
					 vp->name, vp->name_length, vp);
			logmsg(LOG_CRIT,
			       "server %s: could not find variable %s!",
			       srvid(srv), buf);
			abort();
		}

		switch (varinst_assign(inst, vp, srv)) {
		case E_VARINST_OK:
			inst->vi_flags |= SLV_VIF_SET;
			if (debug_level[SLB_DEBCAT_SNMP] >= 3) {
				snprint_variable(buf, sizeof(buf),
						 vp->name, vp->name_length,
						 vp);
				debug_printf("%s: %s = %s",
					     srvid(srv), inst->vi_sym.name,
					     buf);
			}
			break;

		case E_VARINST_UNSUPPORTED:
			break;

		case E_VARINST_OVERFLOW:
			slb_node_state_init(srv->state);
			srv->expr->ex_eval_count = 0;
			varinst_clear(srv->varinst);
			return;
		}
	}

	if (varinst_allset(srv->varinst)) {
		debug(SLB_DEBCAT_SNMP, 3, ("%s: evaluating expression: \"%s\"",
			  srvid(srv), srv->expr->ex_text));
		if (eval_expression(srv->expr, &srv->state, &srv->weight)
		    == 0) {
			srv->flags |= SLB_SRV_COMPUTED;
			debug(SLB_DEBCAT_SNMP, 3, ("%s: expression yields %g",
				  srvid(srv), srv->weight));
		}
	}
}

static int
slb_table_grow(struct slb_table *tab, struct variable_list *vp)
{
	char *buf;
	struct slb_idxnum *ent;
	int install = 1;
	int ovf;
        size_t out_len;

	buf = slb_format_oid_value(vp->name, vp->name_length, vp, NULL);
	if (!buf) {
		logmsg(LOG_ERR,
		       "cannot format variable: %s",
		       snmp_api_errstring(snmp_errno));
		return 1;
	}
	
	ent = idxnum_lookupz(tab->tab_idx, buf, &install);
	free(buf);
	if (ent->idxnum_strval) {
		ent->idxnum_strsiz = 16;
		ent->idxnum_strval = grecs_malloc(ent->idxnum_strsiz);
	}
	ovf = 0;
	out_len = 0;
	netsnmp_sprint_realloc_objid(&ent->idxnum_strval,
				     &ent->idxnum_strsiz,
				     &out_len, 1,
				     &ovf,
				     vp->name + tab->tab_oidlen,
				     vp->name_length - tab->tab_oidlen);
	return 0;
}

struct table_match_closure {
	struct grecs_symtab *tables;
	oid *oid;
	size_t oidlen;
	struct slb_table *result;
};

static int
table_match_var(void *sym, void *data)
{
	struct slb_table *tab = sym;
	struct table_match_closure *clos = data;

	if (clos->oidlen >= tab->tab_oidlen
	    && memcmp(tab->tab_oid, clos->oid,
		      tab->tab_oidlen * sizeof(clos->oid[0])) == 0) {
		/* Select longest matching prefix */
		if (!clos->result ||
		    clos->result->tab_oidlen < tab->tab_oidlen)
			clos->result = tab;
	}
	return 0;
}
	
static struct slb_table *
oid_to_table(struct slb_server *srv, oid *oid, size_t oidlen)
{
	struct table_match_closure clos;

	clos.tables = srv->tables;
	clos.oid = oid;
	clos.oidlen = oidlen;
	clos.result = NULL;
	grecs_symtab_foreach(srv->tables, table_match_var, &clos);
	return clos.result;
}

static int
resolve_tabref(struct slb_varinstance *vinst, struct slb_server *srv)
{
	struct slb_tabref *ref = vinst->vi_tabref;
	struct grecs_txtacc *acc;
	size_t i, j;
	unsigned char *buf;
	size_t buf_len = 256;
	size_t out_len = 0;
	int ovf = 0;
	char *mib;
	
	acc = grecs_txtacc_create();
	for (i = j = 0; i < vinst->vi_wordc; i++) {
		if (i)
			grecs_txtacc_grow_char(acc, '.');
		if (i == ref[j].tr_idxpos) {
			struct slb_idxnum *ent =
			   idxnum_lookupz(ref[j].tr_index->idx_table->tab_idx,
					  ref[j].tr_index->idx_tag,
					  NULL);
			if (!ent) {
				logmsg(LOG_NOTICE,
				       "%s:%d: cannot resolve %s (index %s); "
				       "disabling server %s",
				       vinst->vi_locus.beg.file,
				       vinst->vi_locus.beg.line,
				       vinst->vi_mib,
				       ref[j].tr_index->idx_tag,
				       srvid(srv));
				srv->flags |= SLB_SRV_DISABLED;
				return 1;
			}
			grecs_txtacc_grow(acc, ent->idxnum_strval + 1,
					  strlen(ent->idxnum_strval + 1));
		} else {
			grecs_txtacc_grow(acc, vinst->vi_wordv[i],
					  strlen(vinst->vi_wordv[i]));
		}
	}
	grecs_txtacc_grow_char(acc, 0);
	mib = grecs_txtacc_finish(acc, 0);
	vinst->vi_oidlen = MAX_OID_LEN;
	oidtab_delete(srv->oidtab, vinst);
	if (!read_objid(mib, vinst->vi_oid, &vinst->vi_oidlen)) {
		logmsg(LOG_NOTICE,
		       "%s:%d: cannot parse %s (MIB %s); "
		       "disabling server %s",
		       vinst->vi_locus.beg.file,
		       vinst->vi_locus.beg.line,
		       vinst->vi_mib,
		       mib,
		       ref[j].tr_index->idx_tag,
		       srvid(srv));
		grecs_txtacc_free(acc);
		srv->flags |= SLB_SRV_DISABLED;
		return 1;		
	}
	
	grecs_txtacc_free(acc);

	buf = grecs_malloc(buf_len);
	netsnmp_sprint_realloc_objid(&buf,
				     &buf_len,
				     &out_len, 1,
				     &ovf,
				     vinst->vi_oid,
				     vinst->vi_oidlen);
				
	logmsg(LOG_INFO,
	       "%s:%d: (%s) %s resolved to %s",
	       vinst->vi_locus.beg.file,
	       vinst->vi_locus.beg.line,
	       srvid(srv),
	       vinst->vi_mib,
	       buf);
	free(buf);
	oidtab_install(srv->oidtab, vinst);
	return 0;
}

static int
resolve_varinst_index(void *sym, void *data)
{
        struct slb_varinstance *vinst = sym;
        struct slb_server *srv = data;

	if (vinst->vi_tabref)
		return resolve_tabref(vinst, srv);
	return 0;
}

#if 0
static int
resolve_assertion_index(void *sym, void *data)
{
        struct slb_assertion *ap = sym;
        struct slb_server *srv = data;
	return 1;//FIXME
}
#endif

static void
process_next(struct slb_server *srv, struct snmp_pdu *pdu)
{
	struct variable_list *vp;
	struct snmp_pdu *pdu2;
	size_t varcnt = 0;

	if (debug_level[SLB_DEBCAT_SNMP] >= 2 &&
	    !(srv->flags & SLB_SRV_DEBUG_OUTPUT)) {
		debug_printf("%s replied", srvid(srv));
		srv->flags |= SLB_SRV_DEBUG_OUTPUT;
	}
	debug(SLB_DEBCAT_SNMP, 10, ("reply from %s", srvid(srv)));
	
	pdu2 = snmp_clone_pdu(srv->pdu);
	snmp_free_varbind(pdu2->variables);
	pdu2->variables = NULL;
	srv->pdu = pdu2;
	
	for (vp = pdu->variables; vp; vp = vp->next_variable) {
		struct slb_table *tab =
			oid_to_table(srv, vp->name, vp->name_length);
		if (!tab)
			continue;
		slb_table_grow(tab, vp);
		snmp_add_null_var(pdu2, vp->name, vp->name_length);
		varcnt++;		
	}
	
	if (varcnt && snmp_send(srv->sess, pdu2))
		active_count++;
	else {
		snmp_free_pdu(pdu2);
		debug(SLB_DEBCAT_SNMP, 2,
		      ("%s: resolving indexed variables", srvid(srv)));
		grecs_symtab_foreach(srv->varinst,
				     resolve_varinst_index,
				     srv);
#if 0
		grecs_symtab_foreach(srv->assertions,
				     resolve_assertion_index,
				     srv);
#endif
	}
}

static void
_process_pdu(int operation, struct slb_server *srv,
	     struct snmp_pdu *pdu,
	     void (*proc)(struct slb_server *, struct snmp_pdu *),
	     int clrflag)
{
	if (operation == NETSNMP_CALLBACK_OP_RECEIVED_MESSAGE) {
		proc(srv, pdu);
	} else {
		logmsg(LOG_ERR, "server %s: timeout", srvid(srv));
		slb_server_init(srv, clrflag);
	}
}

void
server_process_pdu(int operation, struct slb_server *srv,
		   struct snmp_pdu *pdu)
{
	_process_pdu(operation, srv, pdu, process_result, 0);
}

int
asynch_response(int operation, struct snmp_session *sp, int reqid,
		struct snmp_pdu *pdu, void *magic)
{
	struct slb_server *srv = magic;

	active_count--;
	_process_pdu(operation, srv, pdu, process_result, 0);
	return 1;
}

int
asynch_response_next(int operation, struct snmp_session *sp, int reqid,
		     struct snmp_pdu *pdu, void *magic)
{
	struct slb_server *srv = magic;

	active_count--;
	_process_pdu(operation, srv, pdu, process_next,
		     SLB_SRV_TAB_RESOLVED);
	return 1;
}

static int
add_snmp_var(void *sym, void *data)
{
	struct slb_varinstance *vinst = sym;
	struct snmp_pdu *req = data;

	if ((vinst->vi_flags & (SLV_VIF_SNMP|SLV_VIF_USED)) ==
	    (SLV_VIF_SNMP|SLV_VIF_USED))
		snmp_add_null_var(req, vinst->vi_oid, vinst->vi_oidlen);
	return 0;
}

static int
add_snmp_assertion(void *sym, void *data)
{
	struct slb_assertion *ap = sym;
	struct snmp_pdu *req = data;

	snmp_add_null_var(req, ap->name, ap->length);
	return 0;
}

static int
add_table_var(void *sym, void *data)
{
	struct slb_table *tab = sym;
	struct snmp_pdu *req = data;
	snmp_add_null_var(req, tab->tab_oid, tab->tab_oidlen);
	return 0;
}

static void
sess_init()
{
	struct slb_server *srv;
	
	for (srv = srv_head; srv; srv = srv->next)
		srv->flags &= ~(SLB_SRV_COMPUTED|SLB_SRV_DEBUG_OUTPUT);
}

static int
srv_open_session(struct slb_server *srv)
{
	struct snmp_session sess;

	snmp_sess_init(&sess);
	sess.version = srv->snmpver;
	sess.peername = srv->peer;
	if (srv->community) {
		sess.community = srv->community;
		sess.community_len = strlen((char*)sess.community);
	}
	if (srv->timeout)
		sess.timeout = srv->timeout * 1000000L;
	if (srv->retries)
		sess.retries = srv->retries;
	if (sess.version == SNMP_VERSION_3) {
		sess.securityAuthProto = srv->snmp_v3.auth.proto;
		sess.securityAuthProtoLen = srv->snmp_v3.auth.proto_len;
		memcpy(sess.securityAuthKey, 
		       srv->snmp_v3.auth.key,
		       srv->snmp_v3.auth.key_len);
		sess.securityAuthKeyLen = srv->snmp_v3.auth.key_len;
			
		sess.securityPrivProto = srv->snmp_v3.priv.proto;
		sess.securityPrivProtoLen = srv->snmp_v3.priv.proto_len;
		memcpy(sess.securityPrivKey,
		       srv->snmp_v3.priv.key,
		       srv->snmp_v3.priv.key_len);
		sess.securityPrivKeyLen = srv->snmp_v3.priv.key_len;
		if (srv->snmp_v3.user) {
			sess.securityName = srv->snmp_v3.user;
			sess.securityNameLen = strlen(sess.securityName);
		}
		if (srv->snmp_v3.context_name) {
			sess.contextName = srv->snmp_v3.context_name;
			sess.contextNameLen =
				strlen(srv->snmp_v3.context_name);
		}
		if (srv->snmp_v3.security_engine_id) {
			sess.securityEngineID =
				srv->snmp_v3.security_engine_id;
			sess.securityEngineIDLen =
				srv->snmp_v3.security_engine_id_len;
		}
		if (srv->snmp_v3.context_engine_id) {
			sess.contextEngineID =
				srv->snmp_v3.context_engine_id;
			sess.contextEngineIDLen =
				srv->snmp_v3.context_engine_id_len;
		}
		sess.securityLevel = srv->snmp_v3.security_level;
		sess.engineBoots = srv->snmp_v3.engine_boots;
		sess.engineTime = srv->snmp_v3.engine_time;
	}
	sess.callback_magic = srv;

	if (!(srv->sess = snmp_open(&sess))) {
		logmsg(LOG_ERR,
		       "server %s: snmp_open failed: %s",
		       srvid(srv),
		       snmp_api_errstring(snmp_errno));
		return 1;
	}
	return 0;
}

static size_t
send_requests()
{
	struct slb_server *srv;
	size_t refcnt = 0;
	
	debug(SLB_DEBCAT_SNMP, 1, ("sending requests"));
	for (srv = srv_head; srv; srv = srv->next) {
		struct snmp_pdu *req;

		if (srv->flags & (SLB_SRV_DISABLED|SLB_SRV_COMPUTED))
			continue;
		
		debug(SLB_DEBCAT_SNMP, 2,
		      ("server %s, host %s", srvid(srv), srv->peer));

		if (!srv->sess && srv_open_session(srv))
			continue;

		if ((srv->flags & SLB_SRV_TAB_REFERENCED) &&
		    !(srv->flags & SLB_SRV_TAB_RESOLVED)) {
			srv->sess->callback = asynch_response_next;
			refcnt++;

			req = snmp_pdu_create(SNMP_MSG_GETNEXT);
			grecs_symtab_foreach(srv->tables, add_table_var, req);
			srv->flags |= SLB_SRV_TAB_RESOLVED;
		} else {
			srv->sess->callback = asynch_response;
			varinst_unset(srv->varinst);
			
			req = snmp_pdu_create(SNMP_MSG_GET);
			grecs_symtab_foreach(srv->varinst, add_snmp_var, req);
			grecs_symtab_foreach(srv->assertions, 
                                             add_snmp_assertion, req);
		}
		
		if (snmp_send(srv->sess, req)) {
			active_count++;
			srv->pdu = req;
		} else {
			logmsg(LOG_ERR, "server %s: snmp_send failed: %s",
			       srvid(srv), snmp_api_errstring(snmp_errno));
			snmp_free_pdu(req);
			snmp_close(srv->sess);
			srv->sess = NULL;
		}
	}
	return refcnt;
}

static void
recv_loop()
{
	slb_loop_serial++;
	slb_loop_ts = time(NULL);
	debug(SLB_DEBCAT_SNMP, 1, ("waiting for replies"));
	while (active_count) {
		int fds = 0, block = 1;
		fd_set fdset;
		struct timeval timeout;

		FD_ZERO(&fdset);
		snmp_select_info(&fds, &fdset, &timeout, &block);
		fds = select(fds, &fdset, NULL, NULL, block ? NULL : &timeout);
		if (fds > 0)
			snmp_read(&fdset);
		else
			snmp_timeout();
	}
}

static void
sess_cleanup()
{
	struct slb_server *srv;
	
	for (srv = srv_head; srv; srv = srv->next) {
		if (srv->sess) {
			snmp_close(srv->sess);
			srv->sess = NULL;
		}
	}
}

static int
srvcmp(const void *a, const void *b)
{
	struct slb_server const *sa = *(struct slb_server const **)a;
	struct slb_server const *sb = *(struct slb_server const **)b;

	if (sa->weight < sb->weight)
		return -1;
	if (sa->weight > sb->weight)
		return 1;
	return 0;
}

void
sort()
{
	struct slb_server *srv;
	size_t i;

	if (!srvtab)
		srvtab = grecs_calloc(srv_count, sizeof(*srv));
	
	for (srv = srv_head, i = 0; srv; srv = srv->next) {
		if (srv->flags & SLB_SRV_COMPUTED)
			srvtab[i++] = srv;
	}
	srvtab_count = i;
	
	qsort(srvtab, srvtab_count, sizeof(srvtab[0]), srvcmp);

	debug(SLB_DEBCAT_SNMP, 1,
	      ("load table computed (%d entries)", srvtab_count));
	
	if (debug_level[SLB_DEBCAT_SNMP] >= 4) {
		debug_printf("servers by load:");
		for (i = 0; i < srvtab_count; i++)
			debug_printf("%s %g", srvid(srvtab[i]),
				     srvtab[i]->weight);
	}
}
	

void
snmploop()
{
	active_count = 0;
	sess_init();
	if (send_requests()) {
		/* Process tables */
		debug(SLB_DEBCAT_SNMP, 2, ("getting subtrees"));
		recv_loop();
		send_requests();
	}
	recv_loop();
	sess_cleanup();
	sort();
}
