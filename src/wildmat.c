/* This file is part of SLB
   Copyright (C) 2011, 2012 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"
#include "wordsplit.h"

#define WILD_FALSE 0
#define WILD_TRUE  1
#define WILD_ABORT 2

int
match_char_class(char **pexpr, char c, int icase)
{
	int res;
	int rc;
	char *expr = *pexpr;

	if (icase)
		c = toupper(c);
	
	expr++;
	if (*expr == '^') {
		res = 0;
		expr++;
	} else
		res = 1;

	if (*expr == '-' || *expr == ']')
		rc = c == *expr++;
	else
		rc = !res;
	
	for (; *expr && *expr != ']'; expr++) {
		if (rc == res) {
			if (*expr == '\\' && expr[1] == ']')
				expr++;
		} else if (expr[1] == '-') {
			if (*expr == '\\')
				rc = *++expr == c;
			else {
				if (icase)
					rc = toupper(*expr) <= c &&
						c <= toupper(expr[2]);
				else
					rc = *expr <= c && c <= expr[2];
				expr += 2;
			}
		} else if (*expr == '\\' && expr[1] == ']')
			rc = *++expr == c;
		else if (icase)
			rc = toupper(*expr) == c;
		else
			rc = *expr == c;
	}
	*pexpr = *expr ? expr + 1 : expr;
	return rc == res;
}

int
_wild_match(char *expr, char *name, int icase)
{
        int c;
        
        while (expr && *expr) {
		if (*name == 0 && *expr != '*')
			return WILD_ABORT;
                switch (*expr) {
                case '*':
			while (*++expr == '*')
				;
			if (*expr == 0)
				return WILD_TRUE;
			while (*name) {
				int res = _wild_match(expr, name++, icase);
				if (res != WILD_FALSE)
					return res;
			}
                        return WILD_ABORT;
                        
                case '?':
                        expr++;
                        if (*name == 0)
                                return WILD_FALSE;
                        name++;
                        break;
                        
		case '[':
			if (!match_char_class(&expr, *name, icase))
				return WILD_FALSE;
			name++;
			break;
			
                case '\\':
                        if (expr[1]) {
				c = *++expr; expr++;
				if (*name != wordsplit_c_unquote_char(c))
					return WILD_FALSE;
				name++;
				break;
			}
			/* fall through */
                default:
			if (icase) {
				if (toupper(*expr) != toupper(*name))
					return WILD_FALSE;
			} else if (*expr != *name)
                                return WILD_FALSE;
                        expr++;
                        name++;
                }
        }
        return *name == 0;
}

int
wildmatch(char *expr, char *name, int icase)
{
	return _wild_match(expr, name, icase) != WILD_TRUE;
}
