/* This file is part of SLB
   Copyright (C) 2011, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"

struct slb_oidref
{
	struct slb_varinstance const *inst;
};

static unsigned
oidref_hash(void *entry, unsigned long hash_size)
{
	struct slb_oidref *ref = entry;
	int i;
	unsigned hash = 0;

	for (i = 0; i < ref->inst->vi_oidlen; i++) {
		hash <<= 1;
		hash ^= ref->inst->vi_oid[i];
	}
	return hash % hash_size;
}

static int
oidref_cmp(const void *a, const void *b)
{
	const struct slb_oidref *aref = a;
	const struct slb_oidref *bref = b;

	if (aref->inst == bref->inst)
		return 0;
	return snmp_oid_compare(aref->inst->vi_oid, aref->inst->vi_oidlen,
				bref->inst->vi_oid, bref->inst->vi_oidlen);
}

static int
oidref_copy(void *a, void *b)
{
	struct slb_oidref *aref = a;
	struct slb_oidref *bref = b;

	aref->inst = bref->inst;
	return 0;
}

void
oidref_free(void *p)
{
	free(p);
}

int
oidtab_install(struct grecs_symtab *oidtab, struct slb_varinstance *vinst)
{
	struct slb_oidref key;
	int install = 1;
	
	key.inst = vinst;
	return grecs_symtab_lookup_or_install(oidtab, &key, &install) == NULL;
}	

int
oidtab_delete(struct grecs_symtab *oidtab, struct slb_varinstance *vinst)
{
	struct slb_oidref key;
	int install = 1;
	
	key.inst = vinst;
	return grecs_symtab_remove(oidtab, &key);
}	

static int
_create_ref(void *sym, void *data)
{
	struct slb_varinstance *vinst = sym;
	struct grecs_symtab *oidtab = data;
	return oidtab_install(oidtab, vinst);
}
	
struct grecs_symtab *
create_oidtab(struct grecs_symtab *vit)
{
	struct grecs_symtab *oidtab;

	oidtab = grecs_symtab_create(sizeof(struct slb_oidref),
				     oidref_hash, oidref_cmp, oidref_copy,
				     NULL,
				     oidref_free);
	if (!oidtab)
		grecs_alloc_die();

	grecs_symtab_foreach(vit, _create_ref, oidtab);
	return oidtab;
}

struct slb_varinstance *
oidtab_lookup(struct grecs_symtab *oidtab, oid *name, size_t name_length)
{
	struct slb_varinstance key_inst;
	struct slb_oidref key, *ref;

	memcpy(key_inst.vi_oid, name, sizeof(name[0]) * name_length);
	key_inst.vi_oidlen = name_length;
	key.inst = &key_inst;
	ref = grecs_symtab_lookup_or_install(oidtab, &key, NULL);
	if (!ref) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("lookup in oidtab failed: %s"),
			       grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return (struct slb_varinstance *)ref->inst;
}

static unsigned
assertion_hash(void *entry, unsigned long hash_size)
{
	struct slb_assertion *ap = entry;
	int i;
	unsigned hash = 0;

	for (i = 0; i < ap->length; i++) {
		hash <<= 1;
		hash ^= ap->name[i];
	}
	return hash % hash_size;
}

static int
assertion_cmp(const void *a, const void *b)
{
	const struct slb_assertion *ap = a;
	const struct slb_assertion *bp = b;

	return snmp_oid_compare(ap->name, ap->length, bp->name, bp->length);
}

static int
assertion_copy(void *a, void *b)
{
	struct slb_assertion *ap = a;
	struct slb_assertion *bp = b;

	memcpy(ap->name, bp->name, sizeof(ap->name[0]) * bp->length);
	ap->length = bp->length;
	return 0;
}

void
assertion_free(void *p)
{
	struct slb_assertion *ap = p;
	free(ap->value);
	free(p);
}

struct grecs_symtab *
assertion_symtab_create(void)
{
	struct grecs_symtab *oidtab;

	oidtab = grecs_symtab_create(sizeof(struct slb_assertion),
			       assertion_hash, assertion_cmp,
			       assertion_copy, NULL,
			       assertion_free);
	if (!oidtab)
		grecs_alloc_die();
	return oidtab;
}

struct slb_assertion *
assertion_lookup(struct grecs_symtab *at, oid *name, size_t namelen, int *install)
{
	struct slb_assertion key, *ref;
	
	memcpy(key.name, name, sizeof(name[0]) * namelen);
	key.length = namelen;
	ref = grecs_symtab_lookup_or_install(at, &key, install);
	if (!ref) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("lookup in oidtab failed: %s"),
			       grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return ref;
}



		
