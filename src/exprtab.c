/* This file is part of SLB
   Copyright (C) 2011, 2019 Sergey Poznyakoff

   SLB is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   SLB is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with SLB.  If not, see <http://www.gnu.org/licenses/>. */

#include "slb.h"

/* Register an expression. */
int
symtab_register_expression(struct grecs_symtab **pexptab,
			   struct slb_expression *expr)
{
	struct slb_expression *s;
	int install = 1;
	struct grecs_symtab *exptab;
	
	if (!*pexptab) {
		*pexptab =
		    grecs_symtab_create_default(sizeof(struct slb_expression));
		if (!*pexptab)
			grecs_alloc_die();
	}
	exptab = *pexptab;
	
	s = grecs_symtab_lookup_or_install(exptab, expr, &install);
	if (!s) {
		logmsg(LOG_CRIT, _("cannot register expression: %s"),
		       grecs_symtab_strerror(errno));
		exit(EX_SOFTWARE);
	}

	if (!install) {
		grecs_error(&expr->ex_locus, 0,
			    _("redefinition of expression `%s'"),
			    expr->ex_name);
		grecs_error(&s->ex_locus, 0,
			    _("previously defined here"));
		return 1;
	}

	memcpy((char*)s + offsetof(struct slb_expression, ex_text),
	       (char*)expr + offsetof(struct slb_expression, ex_text),
	       sizeof(struct slb_expression) -
	       offsetof(struct slb_expression, ex_text));

	return 0;
}

struct slb_expression *
symtab_expression_lookupz(struct grecs_symtab *exptab, const char *name)
{
	struct slb_expression ex, *s;
	
	if (!exptab)
		return NULL;
	ex.ex_name = name;

	s = grecs_symtab_lookup_or_install(exptab, &ex, NULL);
	if (!s) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("expression lookup: %s"),
			       grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return s;
}

struct slb_expression *
symtab_expression_lookup(struct grecs_symtab *exptab,
			 const char *name, size_t len)
{
	struct slb_expression ex, *s;
	static char *namebuf;
	static size_t namelen;
	
	if (!exptab)
		return NULL;
	if (len + 1 >= namelen) {
		namelen = len + 1;
		namebuf = grecs_realloc(namebuf, namelen);
	}
	memcpy(namebuf, name, len);
	namebuf[len] = 0;
	ex.ex_name = namebuf;

	s = grecs_symtab_lookup_or_install(exptab, &ex, NULL);
	if (!s) {
		if (errno == ENOENT)
			return NULL;
		else {
			logmsg(LOG_CRIT, _("expression lookup: %s"),
			       grecs_symtab_strerror(errno));
			exit(EX_SOFTWARE);
		}
	}
	return s;
}

static struct grecs_symtab *expr_table;

int
register_expression(struct slb_expression *expr)
{
	return symtab_register_expression(&expr_table, expr);
}

struct slb_expression *
expression_lookupz(const char *name)
{
	return symtab_expression_lookupz(expr_table, name);
}

struct slb_expression *
expression_lookup(const char *name, size_t len)
{
	return symtab_expression_lookup(expr_table, name, len);
}


struct slb_expression *
copy_expression(struct slb_expression *src, struct grecs_symtab *vt)
{
	struct slb_expression *dst = grecs_malloc(sizeof(*dst));
	memcpy(dst, src, sizeof(*dst));
	if (vt)
		dst->ex_vartab = vt;
	else
		dst->ex_vartab =
			grecs_symtab_create_default(sizeof(struct slb_varref));
	dst->ex_tree = copy_node(src->ex_tree, dst->ex_vartab);
	return dst;
}


#define MAX(a,b) (((a) > (b)) ? (a) : (b))

static void
nodefixup(struct slb_node *node, struct grecs_symtab *vit,
	  struct slb_expression *expr)
{
	struct slb_varinstance *vi;
	struct slb_node *np;
	int min_eval = 0;
	
	switch (node->n_type) {
	case node_unknown:
		abort();
		
	case node_variable:
		vi = varinst_lookupz(vit, node->v.n_var->vr_sym.name, NULL);
		if (vi) {
			node->v.n_var->vr_inst = vi;
			if (!(vi->vi_flags & (SLV_VIF_SNMP|SLV_VIF_CONST))) {
				if (!(vi->vi_flags & SLV_VIF_SET)) {
					grecs_error(&node->n_locus, 0,
						    "use of uninitialized "
						    "variable %s",
						    vi->vi_sym.name);
				}
			}
			vi->vi_flags |= SLV_VIF_USED;
		}
		break;

	case node_uminus:
	case node_not:
		nodefixup(node->v.n_arg[0], vit, expr);
		node->n_min_eval = node->v.n_arg[0]->n_min_eval;
		break;
		
	case node_add:
	case node_sub:
	case node_mul:
	case node_div:
	case node_or:
	case node_and:
	case node_eq:
	case node_ne:
 	case node_lt:
	case node_le:
	case node_gt:
	case node_ge:
	case node_pow:
		nodefixup(node->v.n_arg[0], vit, expr);
		nodefixup(node->v.n_arg[1], vit, expr);
		node->n_min_eval = MAX(node->v.n_arg[0]->n_min_eval,
				       node->v.n_arg[1]->n_min_eval);
		break;
		
 	case node_cond:
		nodefixup(node->v.n_arg[0], vit, expr);
		min_eval = node->v.n_arg[0]->n_min_eval;
		if (node->v.n_arg[1]) {
			nodefixup(node->v.n_arg[1], vit, expr);
			node->n_min_eval = MAX(min_eval,
					       node->v.n_arg[1]->n_min_eval);
		}
		nodefixup(node->v.n_arg[2], vit, expr);
		node->n_min_eval = MAX(min_eval,
				       node->v.n_arg[2]->n_min_eval);
		break;

	case node_func:
		min_eval = 0;
		for (np = node->v.n_func.args; np; np = np->n_next) {
			nodefixup(np, vit, expr);
			if (np->n_min_eval > min_eval)
				min_eval = np->n_min_eval;
		}
		node->n_min_eval += min_eval;
		break;
		
	case node_const:
		break;

	case node_subexpr: {
		struct slb_expression *subexpr =
			expression_lookupz(node->v.n_expr.name);
		if (!subexpr) {
			grecs_error(&node->n_locus, 0, 
				    "unknown expression %s",
				    node->v.n_expr.name);
			return;
		}
		if (!node->v.n_expr.expr) {
			node->v.n_expr.expr = copy_expression(subexpr,
							      expr->ex_vartab);
			fixup_expression(node->v.n_expr.expr, vit);
			node->n_min_eval = node->v.n_expr.expr->ex_tree->n_min_eval;
		}
		break;
	}
		
	case node_asgn:
		nodefixup(node->v.n_asgn.expr, vit, expr);
		node->n_min_eval = node->v.n_asgn.expr->n_min_eval;
		vi = varinst_lookupz(vit, node->v.n_asgn.var->vr_sym.name,
				     NULL);
		if (vi) {
			node->v.n_asgn.var->vr_inst = vi;
			vi->vi_flags |= SLV_VIF_USED | SLV_VIF_SET;
		}
		break;

	case node_list:
		for (np = node->n_next; np; np = np->n_next) {
			nodefixup(np, vit, expr);
			if (np->n_min_eval > node->n_min_eval)
				node->n_min_eval = np->n_min_eval;
		}
		break;
	}
}

struct report_undef_closure
{
	grecs_locus_t *loc;
	int rc;
};

static int
report_undefined_refs(void *sym, void *data)
{
	struct slb_varref *ref = sym;
	struct report_undef_closure *clos = data;
	
	if (!ref->vr_inst) {
		grecs_error(clos->loc, 0,
			    _("variable or constant %s not defined"),
			    ref->vr_sym.name);
		clos->rc = 1;
	}
	return 0;
}

static int
mark_used_instances(void *sym, void *data)
{
	struct slb_varinstance *vinst = sym;
	grecs_locus_t *loc = data;

	bind_output_refname(vinst);
	if (!(vinst->vi_flags & SLV_VIF_USED)) {
		grecs_warning(loc, 0,
			      _("variable %s is never used"),
			      vinst->vi_sym.name);
	}
	return 0;
}

void
bind_expression(struct slb_expression *expr, struct grecs_symtab *vit)
{
	nodefixup(expr->ex_tree, vit, expr);
}

int
fixup_expression(struct slb_expression *expr, struct grecs_symtab *vit)
{
	struct report_undef_closure clos;

        if (expr->ex_fixed)
                return 0;
	nodefixup(expr->ex_tree, vit, expr);
	clos.rc = grecs_error_count;
	clos.loc = &expr->ex_locus;
	grecs_symtab_foreach(expr->ex_vartab, report_undefined_refs, &clos);
	undo_output_refs();
	grecs_symtab_foreach(vit, mark_used_instances, &expr->ex_locus);
	return clos.rc;
}
