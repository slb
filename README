SLB README
Copyright (C) 2011, 2012 Sergey Poznyakoff
See the end of file for copying conditions.

* Introduction

This file contains brief information about configuring and installing
SLB. It is *not* intended as a replacement for the documentation, and
is provided as a brief reference only.  The complete documentation for
Mailfromd is available in doc/ subdirectory in both manpage and
texinfo formats.  The latter is authoritative.  To read it without
installing the package run `info -f doc/slb.info'.  After the package
is installed the documentation can be accessed via `info slb'.

An online copy of the documentation in various formats is available
at http://slb.man.gnu.org.ua.

* Overview

SLB stands for "Simple Load Balancer".  The utility monitors a
set of remote servers, obtaining a set of numeric values via SNMP.
These values are used to compute a single floating-point number,
called a "relative load" for that server.  The servers are then sorted
in order of increasing loads and the resulting table is printed (using
a configurable format string) to a file or pipe.

* Prerequisites

This package requires Net-SNMP version 5.5 or newer (http://www.net-snmp.org).

* Installation

Configure and make:

   ./configure [OPTIONS]
   make
   make install
  
See the file INSTALL for the description of ./configure and its
generic options.

Options and variables, specific for SLB are:

  --without-preprocessor  disables the use of external preprocessor for
                          SLB configuration file.

By default, "m4 -s" is assumed.  You can change this by setting the
environment variable DEFAULT_PREPROCESSOR to the desired preprocessor
name (with command line arguments).

* Bug reporting.		

Send bug reports to <gray+slb@gnu.org.ua>. 


* Copyright information:

Copyright (C) 2011, 2012 Sergey Poznyakoff

   Permission is granted to anyone to make or distribute verbatim copies
   of this document as received, in any medium, provided that the
   copyright notice and this permission notice are preserved,
   thus giving the recipient permission to redistribute in turn.

   Permission is granted to distribute modified versions
   of this document, or of portions of it,
   under the above conditions, provided also that they
   carry prominent notices stating who last changed them.


Local Variables:
mode: outline
paragraph-separate: "[ 	]*$"
version-control: never
End:
